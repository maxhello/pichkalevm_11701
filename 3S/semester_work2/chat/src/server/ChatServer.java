package server;

import network.TCPConnection;
import network.TCPConnectionListener;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ChatServer implements TCPConnectionListener {
	private int counter = 0;
  public static void main(String[] args) {
	  new ChatServer();
  }
  
  private final HashMap<String, TCPConnection> connections = new HashMap<>();
  
  
  private ChatServer( ) {         // server  waits connection and than create tcpConnection with client socket inside. After that put it in the arList.
	  System.out.println("server running..."); // run thread with on receive . If have message than send it to all in collections.
	 try(ServerSocket serverSocket = new ServerSocket(8189)){
		 System.out.println(serverSocket.getInetAddress() + " " + serverSocket.getLocalSocketAddress() + " " + serverSocket.getReuseAddress() + " " + serverSocket.getChannel());
		while(true) {
			try {
				
				new TCPConnection(serverSocket.accept(), this); // wait and after create socket
			} catch (Exception e) {
				System.out.println("TCPConnection exc: " + e);
			}
		}

	} catch (Exception e) {
		throw new RuntimeException(e);
	}
  }


@Override
public synchronized void onConnectionReady(TCPConnection tcpConnection) {
	// TODO Auto-generated method stub

	connections.put(Integer.toString(++counter),tcpConnection);
	System.out.println("4");
	System.out.println("YourA " + counter);
	tcpConnection.sendString("YourA " + counter);
//	sendToAllConnection("Client connected: " + tcpConnection);
}


@Override
public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
	// TODO Auto-generated method stub
	System.out.println("3");
	sendToAllConnection(value);
}


@Override
public synchronized void onDisconnect(TCPConnection tcpConnection) {
	// TODO Auto-generated method stub
//	sendToAllConnection("Client disconnected: " + tcpConnection);
//	connections.remove(tcpConnection);
	connections.values().remove(tcpConnection);
}

@Override
public synchronized void onException(TCPConnection tcpConnection, Exception e) {
	// TODO Auto-generated method stub
	System.out.println("TCPConnection exception: " + e);
}

private void sendToAllConnection(String value) {
	System.out.println(value);
//	final int cnt = connections.size();
	System.out.println(connections.size() + " size");
	for (Map.Entry<String, TCPConnection> entry : connections.entrySet()) {
		System.out.println(entry.getKey() + " " + entry.getValue());
		entry.getValue().sendString(value);

//		connections.get(i).sendString(value);
	}
}
  
  
  
  
  
  
}
