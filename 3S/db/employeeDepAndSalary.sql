///1
select name from class.departament
where id IN(select dep_id from class.employee group by
dep_id having AVG(salary) > 10000);

///2
select name from class.departament
where id IN(select dep_id from class.employee where
salary =(select max(salary) from class.employee));
///3
SELECT DISTINCT A.name AS name1, B.name AS name2
FROM class.employee AS A, class.employee B
WHERE A.manager_id = B.id AND 
 A.salary > B.salary;
///4
SELECT * 
FROM class.employee AS employees
WHERE employees.salary = (SELECT MAX(salary) 
FROM class.employee AS max 
WHERE max.dep_id = employees.dep_id);


1///
select name from class.departament
where id IN(select dep_id from class.employee group by
dep_id having count(name)<3);

2///

with sum_salary as
  ( select dep_id, sum(salary) salary
    from   class.employee
    group  by dep_id )
select dep_id
from   sum_salary a       
where  a.salary = ( select max(salary) from sum_salary);


3///

SELECT DISTINCT A.name AS name1, B.name AS name2
FROM class.employee AS A, class.employee B
WHERE A.manager_id = B.id AND 
 A.dep_id != B.dep_id;


