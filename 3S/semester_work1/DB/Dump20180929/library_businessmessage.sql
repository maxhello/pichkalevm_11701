-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: library
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `businessmessage`
--

DROP TABLE IF EXISTS `businessmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `businessmessage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(200) NOT NULL,
  `unit_id` bigint(20) NOT NULL,
  `proprietor_id` bigint(20) NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  `unit_count` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proprietor_idx` (`proprietor_id`),
  KEY `fk_receiver_idx` (`receiver_id`),
  KEY `fk_unit_idx` (`unit_id`) /*!80000 INVISIBLE */,
  CONSTRAINT `fk_proprietor_id` FOREIGN KEY (`proprietor_id`) REFERENCES `proprietor` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_receiver_idx` FOREIGN KEY (`receiver_id`) REFERENCES `proprietor` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `businessmessage`
--

LOCK TABLES `businessmessage` WRITE;
/*!40000 ALTER TABLE `businessmessage` DISABLE KEYS */;
INSERT INTO `businessmessage` VALUES (1,'he',18,17,18,12,0),(2,'hey, i wanna buy your bread. Will u allow me to do it, bro?2222',26,26,17,1,1),(3,'Hey !',27,17,26,1,0),(4,'PLEASE SELL ME IT',12,26,17,144,0),(5,'Halo',27,17,26,1,0),(6,'wanna buy',14,26,17,3,0),(7,'ZZZ',12,17,26,60,0),(8,'toMax',25,26,17,40,0),(9,'toGreatee2',7,17,26,30,0),(10,'mmm',25,26,17,20,0);
/*!40000 ALTER TABLE `businessmessage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-29  5:54:57
