package dao;


import beans.News;

import java.sql.SQLException;
import java.util.List;

public interface NewsDAO {
    public List<News> getAllNews() throws SQLException;
    public News getNewsById(int id) throws SQLException;
    public void changeNewsById(News news) throws SQLException;
    public void deleteNewsById(int id) throws SQLException;
    public void addNews(News news) throws SQLException;

}
