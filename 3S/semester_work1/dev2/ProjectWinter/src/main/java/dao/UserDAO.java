package dao;

import beans.User;

public interface UserDAO {
    User tryToLogin(String name, String password) throws ClassNotFoundException;
}
