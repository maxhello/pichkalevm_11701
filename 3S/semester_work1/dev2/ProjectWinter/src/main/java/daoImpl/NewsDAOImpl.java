//package daoImpl;
//
//
//import DataBase.Database;
//import beans.News;
//import dao.NewsDAO;
//
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class NewsDAOImpl implements NewsDAO {
//
//    @Override
//    public List<News> getAllNews() throws SQLException {
//
//        List<News> listOfNews = new ArrayList<>();
//        ResultSet rs = null;
//        Connection conn= Database.getConnection();
//        System.out.println(Database.getConnection());
//        System.out.println("i am in getAll DAO");
//        PreparedStatement statement = conn.prepareStatement("select * from book");
//        rs = statement.executeQuery();
//
//       String name = null;
//       String author = null;
//        while (rs.next()) {
//
//            name = rs.getString("name");
//             author = rs.getString("author");
//            System.out.println(name + " name and author " + author);
//            News news = new News(name, author);
//             listOfNews.add(new);
//        }
//        return  listOfNews;
//
//    }
//
//    @Override
//    public News getNewsById(int id) throws SQLException {
//        ResultSet rs = null;
//        Connection conn= Database.getConnection();
//        PreparedStatement statement = conn.prepareStatement("select * from book where id = ?");
//        statement.setInt(1, id);
//
//
//        String name = null;
//        String author = null;
//
//        rs = statement.executeQuery();
//        News book = null;
//        while (rs.next()) {
//
//             author = rs.getString("author");
//             name = rs.getString("name");
//             book = new News(name, author);
//             return book;
//        }
//        return book;
//    }
//
//    @Override
//    public void changeNewsById(News book) throws SQLException {
//
//        PreparedStatement prepStmt = null;
//        ResultSet rs = null;
//        Connection conn = null;
//
//            conn = Database.getConnection();
//
//            prepStmt = conn.prepareStatement("update book set name=?, author=? where id=?");
//            prepStmt.setString(1, book.getName());
//            prepStmt.setString(2, book.getAuthor());
//            prepStmt.executeUpdate();
//
//    }
//
//    @Override
//    public void deleteNewsById(int id) throws SQLException {
//        PreparedStatement prepStmt = null;
//        ResultSet rs = null;
//        Connection conn = null;
//        conn = Database.getConnection();
//        prepStmt = conn.prepareStatement("delete from book where id=?");
//        prepStmt.setInt(1, id);
//        prepStmt.executeUpdate();
//    }
//
//    @Override
//    public void addNews(News news) throws SQLException {
//        PreparedStatement prepStmt = null;
//        ResultSet rs = null;
//        Connection conn = null;
//
//        conn = Database.getConnection();
//
//        prepStmt = conn.prepareStatement("INSERT INTO news (name, author) VALUES (?,?)");
//        prepStmt.setString(1, news.get());
//        prepStmt.setString(2, news.getAuthor());
//        prepStmt.executeUpdate();
//    }
//}
