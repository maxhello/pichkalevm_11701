package daoImpl;
import DataBase.Database;
import beans.News;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewsDAO {

    private ArrayList<News> newsArrayList = new ArrayList<News>();

    public void clearNewsList(){
        newsArrayList.clear();
    }
    private ArrayList<News> getNews(String sql) {

        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                News news = new News();
                news.setId(rs.getLong("id"));
                news.setTitle(rs.getString("title"));
                news.setText(rs.getString("text"));
                news.setAuthor_id(rs.getLong("author_id"));
                news.setImage(rs.getBytes("image"));
                news.setDateOfPublication(rs.getDate("data"));
                newsArrayList.add(news);
            }

        } catch (SQLException ex) {
            Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                tryToCloseConnection(stmt, rs, conn);
            } catch (SQLException ex) {
                Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return newsArrayList;
    }

    private News getOneNews(String sql) {

        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        News news = null;
        try {
            conn = Database.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                news = new News();
                news.setId(rs.getLong("id"));
                news.setTitle(rs.getString("title"));
                news.setText(rs.getString("text"));
                news.setAuthor_id(rs.getLong("author_id"));
                news.setImage(rs.getBytes("image"));
                news.setDateOfPublication(rs.getDate("data"));
                newsArrayList.add(news);
            }

        } catch (SQLException ex) {
            Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                tryToCloseConnection(stmt, rs, conn);
            } catch (SQLException ex) {
                Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return news;
    }

    public ArrayList<News> getAllNews() {
        return getNews("select * from winterproject.news");
    }




    public String updateNews(News news) {

        PreparedStatement prepStmt = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            conn = Database.getConnection();
//            prepStmt = conn.prepareStatement("select * from users where name = ?");
//            prepStmt.setString(1, news.getName());
//
//            rs = prepStmt.executeQuery();
//            int id = 8;
//            while (rs.next()) {
//
//                 id = rs.getInt("id");
//                String name = rs.getString("name");
//                System.out.println("news id : " + id);
//                System.out.println("news name : " + name);
//
//            }

            prepStmt = conn.prepareStatement("update news set author_id=?, text=?, image=?, data=?, title=? where id=?");
            prepStmt.setLong(1, news.getAuthor_id());
            prepStmt.setString(2, news.getText());
            prepStmt.setBytes(3, news.getImage());
            prepStmt.setDate(4, news.getDateOfPublication());
            prepStmt.setString(5, news.getTitle());
            prepStmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                tryToCloseConnection(prepStmt, rs, conn);
            } catch (SQLException ex) {
                Logger.getLogger(NewsDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("update was successful");
        return "books";
    }

    public News getNewsById(int id){
        StringBuilder sql = new StringBuilder("select * from news where id = ?");
        return getOneNews(sql.toString());

    }




    public void tryToCloseConnection(Statement ps, ResultSet rs, Connection cn) throws SQLException {
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
        if (cn != null) {
            cn.close();
        }
    }

}

