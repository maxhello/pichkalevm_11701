package daoImpl;


import DataBase.Database;
import beans.User;
import dao.UserDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserLoginDAO implements UserDAO {
    public UserLoginDAO() {
        super();
    }

    public User tryToLogin(String name, String password) throws ClassNotFoundException {
        try {
            String role = "1";
            String dbName = null;
            String dbPassword = null;
            Byte [] image = null;
            int id = 0;

            String sql = "select * from winterproject.users where name=? and password=?";
            try {

                Class.forName("org.postgresql.Driver");

            } catch (ClassNotFoundException e) {

                System.out.println("Where is your PostgreSQL JDBC Driver? "
                        + "Include in your library path!");
                e.printStackTrace();
            }

            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/winterproject", "postgres", "1234");
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            User user = new User();
            while(rs.next()){
                user.setId(rs.getInt(1));
                user.setName(rs.getString(3));
                dbPassword = rs.getString(4);
                user.setImage(rs.getBytes(5));
                user.setRole(rs.getInt(2));
                System.out.println(user.getName() + "  " + dbPassword +" " + user.getRole() + " HEYYYYYYYYYYYY!");
            }
            System.out.println(user.getName() + "  " + dbPassword +" " + user.getRole() + " HEYYYYYYYYYYYY!");
            if(rs != null) {
              return user;
            } else  return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<User> userList = new ArrayList<User>();

    private ArrayList<User> getUsers() {
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
        try {
            conn = Database.getConnection();

            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from winterproject.users order by name");
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getLong("id"));
                user.setName(rs.getString("name"));
                user.setImage(rs.getBytes("image"));
                user.setRole(rs.getInt("role"));
                userList.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserLoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (stmt!=null) stmt.close();
                if (rs!=null)rs.close();
                if (conn!=null)conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserLoginDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return userList;
    }



    public ArrayList<User> getUserList() {
        if (!userList.isEmpty()) {
            return userList;
        } else {
            return getUsers();
        }
    }

    public User getUserByName(String name){
        if(userList.isEmpty()) userList = this.getUsers();
        for (User user: userList
                ) {
            if(user.getName().equals(name))return user;

        }
        return null;
    }

    public User getUserById(int id){
        if(userList.isEmpty()) userList = this.getUsers();
        for (User user: userList
                ) {
            if(user.getId() == id)return user;

        }
        return null;
    }

}
