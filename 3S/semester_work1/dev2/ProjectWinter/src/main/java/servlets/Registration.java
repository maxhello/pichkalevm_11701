package servlets;

import DataBase.Database;
import org.apache.commons.io.IOUtils;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


@WebServlet("/Registration")
@MultipartConfig(maxFileSize = 16177215)
public class Registration extends javax.servlet.http.HttpServlet {
    private static final long serialVersionUTD =1L;

    public Registration() {
        super();
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        try {
            String name = request.getParameter("user");
            String password = request.getParameter("password");
            String role = "1";
            Part part = request.getPart("image");
            System.out.println(part + "i am in reg");
            InputStream inputStream = null;
            if (part != null)
            {

                System.out.println(part.getName());
                System.out.println(part.getSize());
                System.out.println(part.getContentType());

                inputStream = part.getInputStream();

            }

            String sql = "insert into winterproject.users(name,password,role,image) values (?,?,?,?)";

            InputStream is = part.getInputStream();
            byte[] bytes = IOUtils.toByteArray(is);
            System.out.println(bytes);

            Connection conn = Database.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, password);
            ps.setString(3, role);
            ps.setBytes(4, bytes);
            ps.executeUpdate();

            PrintWriter out = response.getWriter();
            out.println("You was successfully registered");

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(124);
        }
        response.sendRedirect("jspPages/index.jsp");
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }
}
