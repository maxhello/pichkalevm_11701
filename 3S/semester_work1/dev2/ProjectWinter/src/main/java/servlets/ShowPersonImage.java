package servlets;

import beans.User;
import daoImpl.UserLoginDAO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;


public class ShowPersonImage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println(999999999);
        response.setContentType("image/jpeg");
        OutputStream out = response.getOutputStream();
        try {

            UserLoginDAO userList = new UserLoginDAO();
            String name = null;

                name = (String) request.getSession().getAttribute("nameImage");
                System.out.println("name1" + name);
                User user = userList.getUserByName(name);
                response.setContentLength(user.getImage().length);
                out.write(user.getImage());

        }catch (Exception ex){
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
