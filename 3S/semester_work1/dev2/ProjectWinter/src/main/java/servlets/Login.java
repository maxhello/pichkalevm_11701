package servlets;

import beans.User;
import daoImpl.UserLoginDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class Login extends HttpServlet {
    private static final long serialVersionUTD =1L;
    private  UserLoginDAO userLoginDAO;
    public Login() {
        super();
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        userLoginDAO = new UserLoginDAO();
        String name = request.getParameter("user");
        String password = request.getParameter("password");
        System.out.println(name + "   " + password);
        try {
            User user = (userLoginDAO.tryToLogin(name, password));
            System.out.println(user.getName());
            if(user == null){
                {RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                    rd.include(request, response);}
            } else {
                PrintWriter out = response.getWriter();
                out.println("You was successfully logged");
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                session.setAttribute("user_name", user.getName());
                response.sendRedirect("/jspPages/main.jsp");
                session.setAttribute("user_id", user.getId());
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }
}
