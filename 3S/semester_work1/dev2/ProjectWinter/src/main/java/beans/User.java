package beans;

import java.util.ArrayList;
import java.util.Arrays;

public class User {

    private String name;
    private long id;
    private byte[] image;
    private int role;



//    public void refreshResourses(){
//
//    }
public User() {

    }
    public User(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", image=" + Arrays.toString(image) +
                ", role=" + role +
                '}';
    }
}
