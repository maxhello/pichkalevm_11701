<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@ page import="beans.News" %>
<%@ page import="daoImpl.NewsDAO" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%--<jsp:useBean id="bookList" class="somePackage.beans.BookList" scope="page"/>--%>


<div class="book_list">



    <%
        NewsDAO newsDAO = new NewsDAO();
        ArrayList<News> list = newsDAO.getAllNews();

    %>
    <h5 style="text-align: left; margin-top:20px;">Founded news: <%=list.size() %> </h5>
    <%  session.setAttribute("newsList", list);
        for (News news : list) {

    %>

    <div class="book_info">
        <div class="book_title">
            <p> <%=news.getTitle()%></p>
        </div>
        <div class="book_image">
            <a href="#"><img src="<%=request.getContextPath()%>/ShowImage?index=<%=list.indexOf(news)%>" height="180" width="190" alt="Обложка"/></a>
        </div>
        <div class="book_details">
            <br><strong>text:</strong> <%=news.getText()%>
            <br><strong>date:</strong> <%=news.getDateOfPublication()%>
            <br><strong>author:</strong> <%=news.getAuthor_id()%>

        </div>
    </div>
    <%}%>

</div>
