CREATE TABLE "businessmessage" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "comment" varchar(200) NOT NULL,
  "unit_id" bigint(20) NOT NULL,
  "proprietor_id" bigint(20) NOT NULL,
  "receiver_id" bigint(20) NOT NULL,
  "unit_count" int(11) NOT NULL,
  "status" tinyint(1) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "fk_proprietor_idx" ("proprietor_id"),
  KEY "fk_receiver_idx" ("receiver_id"),
  KEY "fk_unit_idx" ("unit_id") /*!80000 INVISIBLE */,
  CONSTRAINT "fk_proprietor_id" FOREIGN KEY ("proprietor_id") REFERENCES "proprietor" ("id") ON UPDATE CASCADE,
  CONSTRAINT "fk_receiver_idx" FOREIGN KEY ("receiver_id") REFERENCES "proprietor" ("id") ON UPDATE CASCADE,
  CONSTRAINT "fk_unit_id" FOREIGN KEY ("unit_id") REFERENCES "unit" ("id") ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci



CREATE TABLE "proprietor" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT,
  "fio" varchar(300) NOT NULL,
  "birthday" date DEFAULT NULL,
  "password" int(11) DEFAULT NULL,
  "image" longblob,
  "credits" bigint(20) DEFAULT NULL,
  "role" int(11) DEFAULT NULL,
  PRIMARY KEY ("id")
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8


INSERT INTO `library`.`businessmessage` (`id`, `comment`, `unit_id`, `proprietor_id`, `receiver_id`, `unit_count`, `status`) VALUES ('', 'hi', '26', '26', '17', '1', '0');
INSERT INTO `library`.`businessmessage` (`comment`, `unit_id`, `proprietor_id`, `receiver_id`, `unit_count`, `status`) VALUES ('hey', '26', '26', '17', '1', '0');
INSERT INTO `library`.`businessmessage` (`comment`, `unit_id`, `proprietor_id`, `receiver_id`, `unit_count`, `status`) VALUES ('hallo', '26', '26', '17', '1', '0');
INSERT INTO `library`.`businessmessage` (`comment`, `unit_id`, `proprietor_id`, `receiver_id`, `unit_count`, `status`) VALUES ('hai', '26', '26', '17', '1', '0');

INSERT INTO `library`.`proprietor` (`fio`, `birthday`, `password`, `credits`, `role`) VALUES ('Greate', '2000', '431', '66666', '2');
INSERT INTO `library`.`proprietor` (`fio`, `birthday`, `password`, `credits`, `role`) VALUES ('Ariovistr', '3', '44321', '44444', '1');
INSERT INTO `library`.`proprietor` (`fio`, `birthday`, `password`, `credits`, `role`) VALUES ('inge', '1999', '654321', '123321', '1');
INSERT INTO `library`.`proprietor` (`fio`, `birthday`, `password`, `credits`, `role`) VALUES ('Tom', '1999', '3333', '3333', '2');

