import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Dispatcher {

    public void hundler(String string){
        switch(string){
//            case "id":
//                Id id = new Id();
//                createPage("id", id.getId());
//                return;


            case "feed":
                Feed feed = new Feed();
                createPage("feed", feed.getFeeds());
                break;

            case "messages":
                Messages messages = new Messages();
                createPage("messages", messages.getMessages());
                break;
            default:
                if (string.matches("Id_[1-9]*")) {
                   Id id = new Id();

                   createPage("id", id.getId());

                }
                break;
        }
    }
    public void createPage(String string, String code){
        String filename = string + ".html";
        File f = new File(filename);
        PrintWriter pw = null;
        try {

            pw = new PrintWriter(f);


            pw.println("<html>");
            pw.println("<head>");
            pw.println("<title>" +string +"</title>");
            pw.println("</head>");
            pw.println("<body>");
            pw.println(code);
            pw.println("</body>");
            pw.println("</html>");

            pw.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally{

            if(pw!=null){
                pw.close();

            }
        }
    }
}
