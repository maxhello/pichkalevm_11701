import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        ClassC cc;
        ClassB cb;
        ClassA ca;
        Class c = ClassC.class;
        Class b = ClassB.class;
        Class a = ClassA.class;
        cc = (ClassC)c.getConstructor().newInstance();
        cb = (ClassB)b.getConstructor().newInstance();
        ca = (ClassA)a.getConstructor().newInstance();


        ArrayList<Object> ar = new ArrayList();
        ar.add(cc);
        ar.add(cb);
        ar.add(ca);

        Method[] methods = ca.getClass().getMethods();


        for(Method method : methods){
          if(isSetter(method)){

              for (Class<?> paramType :  method.getParameterTypes()) {
                  for (Object obj:ar
                       ) {
                      if(obj.getClass().getTypeName().equals(paramType)){
                          method.invoke(obj);
                      }

                  }
              }

              System.out.println("setter: " + method);
          }
        }


    }


    public static boolean isGetter(Method method){
        if(!method.getName().startsWith("get"))      return false;
        if(method.getParameterTypes().length != 0)   return false;
        if(void.class.equals(method.getReturnType())) return false;
        return true;
    }

    public static boolean isSetter(Method method){
        if(!method.getName().startsWith("set")) return false;
        if(method.getParameterTypes().length != 1) return false;
        return true;
    }
}
