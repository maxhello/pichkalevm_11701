public class ClassA {
    ClassC cc;
    ClassB cb;


//    ClassA() throws IllegalAccessException, InstantiationException {
//        Class c = ClassC.class;
//        Class b = ClassB.class;
//        cc = (ClassC)c.newInstance();
//        cb = (ClassB)b.newInstance();
//    }

    public ClassA(){}

    public void setCc(ClassC cc) {
        this.cc = cc;
    }

    public void setCb(ClassB cb) {
        this.cb = cb;
    }
}
