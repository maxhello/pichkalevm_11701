import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Login")
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        String name = request.getParameter("user");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        System.out.println(name + password + role);

        response.setContentType("text/html");
        request.getSession().setAttribute("name", name);
        request.getSession().setAttribute("password", password);
        request.getSession().setAttribute("role", role);

        Cookie cookie = new Cookie("name", name);
        Cookie cookie2 = new Cookie("password", password);
        Cookie cookie3 = new Cookie("role", role);

        response.addCookie(cookie);
        response.addCookie(cookie2);
        response.addCookie(cookie3);
        response.sendRedirect("/Login3");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<form action='/Login2' method=\"get\"> User name : <input type=\"text\" name=\"user\" required=\"required\"> Password password : <input type=\"password\" name=\"password\" required=\"required\"> <input type=\"radio\" name=\"role\" value=\"simpleUser\"> simpleUser<Br>\n" +
                "   <input type=\"radio\" name=\"role\" value=\"admin\"> admin <Br><input type=\"submit\" value=\"LOGIN\">");

        out.close();
    }
}

