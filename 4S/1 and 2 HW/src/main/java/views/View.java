package views;

import objects.User;

import java.util.List;

public interface View {
    String showUser(User user);
    String showUsers();
}
