package views;

import interfaces.UserDao;
import objects.User;
import java.util.List;

public class JSONView implements View {

    private UserDao userDao;
    public UserDao getUserDao() {
        return userDao;
    }

    private String wrapByTd(User user) {
        return ", {\"userame\": " + user.getUsername() +",\"city\": "+ user.getCity()+ ",\"year\": " +user.getYear() + "}" ;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public String showUser(User user) {
        return "{\"userame\": " + user.getUsername() + "}" ;
    }

    @Override
    public String showUsers() {
        List<User> list = userDao.getListOfUsers();
        String result = "{";
        for (User user: list
                ) {
            result += wrapByTd(user);

            result += ", ";
        }
        return result += "}";
    }
}