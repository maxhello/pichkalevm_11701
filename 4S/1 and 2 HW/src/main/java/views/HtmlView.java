package views;

import interfaces.UserDao;
import objects.User;

import java.util.List;

public class HtmlView implements View{
    private UserDao userDao;


    public HtmlView() {
    }

    public HtmlView(UserDao userDao) {
        this.userDao = userDao;
    }


    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    private String wrapByTd(String x) {
        return "<td>" + x + "</td>";
    }


    public String showUsers(){

        String result = "<table>";
        List<User> list = userDao.getListOfUsers();
        for (User user: list
             ) {
            result += "<tr>" + wrapByTd(user.getUsername())
                    + wrapByTd(user.getCity())
                    + wrapByTd("" + user.getYear())
                    + "</tr>";
              }

        return result + "</table>";
    }

    @Override
    public String showUser(User user) {
        return null;
    }
}
