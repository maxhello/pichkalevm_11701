package DAO;


import interfaces.UserDao;
import objects.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PostgresDAO implements UserDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    private DataSource dataSource;

    public void setDataSource() {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public NamedParameterJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

//    public PostgresDAO(DataSource dataSource) {
//        this.dataSource = dataSource;
//        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
//    }

    public PostgresDAO(DataSource dataSource) {
        this.dataSource = dataSource;
          }

    @Override
    public void insert(User user) {
        String sql = "insert into user (name, author) VALUES (:name, :author)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", user.getUsername());
//        params.addValue("author", mp3.getAuthor());
        jdbcTemplate.update(sql, params);
    }


    @Override
    public void insert(List<User> userList) {
        for (User user : userList) {
            insert(user);
        }
    }

    @Override
    public void delete(int id) {
        String sql = "delete from user where id=:id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        jdbcTemplate.update(sql, params);
    }

    @Override
    public void delete(User mp3) {
        delete(mp3.getId());
    }

    @Override
    public User getUserByID(int id) {
        String sql = "select * from user where id=:id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        return jdbcTemplate.queryForObject(sql, params, new MP3RowMapper());
    }

    @Override
    public List<User> getListOfUserByName(String name) {
        String sql = "select * from user where upper(author) like :name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", "%" + name.toUpperCase() + "%");
        return jdbcTemplate.query(sql, params, new MP3RowMapper());
    }

    @Override
    public List<User> getListOfUsers() {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        String sql = "select * from users";
        System.out.println(jdbcTemplate);
        return jdbcTemplate.query(sql, new MP3RowMapper());
    }

    @Override
    public int getMP3Count() {
        String sql = "select count(*) from user";
        return jdbcTemplate.getJdbcOperations().queryForObject(sql, Integer.class);
    }

    private static final class MP3RowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setUsername(rs.getString("username"));
            user.setCity(rs.getString("city"));
            user.setCity(rs.getString("year"));
//            System.out.println(user.getId() + " " + user.getName());
//            System.out.println("hello");

            return user;
        }
    }
}
