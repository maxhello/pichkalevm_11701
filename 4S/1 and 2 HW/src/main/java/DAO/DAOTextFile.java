package DAO;

import interfaces.UserDao;
import objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public  class DAOTextFile implements UserDao {

    private Scanner scanner;
    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }


    @Override
    public void insert(User mp3) {

    }

    @Override
    public void insert(List<User> mp3List) {

    }

    @Override
    public void delete(User mp3) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public User getUserByID(int id) {
        return null;
    }

    @Override
    public List<User> getListOfUsers() {
        List<User> users = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String[] data = scanner.nextLine().split(" ");
            users.add(new User(data[0], data[1], Integer.parseInt(data[2])));
        }
        return users;
    }

    @Override
    public List<User> getListOfUserByName(String author) {
        return null;
    }

    @Override
    public int getMP3Count() {
        return 0;
    }
}
