package interfaces;

import objects.User;

import java.util.List;

public interface UserDao {

	void insert(User mp3);

	void insert(List<User> mp3List);

	void delete(User mp3);

	void delete(int id);

	User getUserByID(int id);

  List<User> getListOfUsers();

	List<User> getListOfUserByName(String author);

	int getMP3Count();

}
