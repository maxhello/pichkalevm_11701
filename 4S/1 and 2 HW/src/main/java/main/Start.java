package main;

import interfaces.UserDao;
import objects.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import views.View;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;


public class Start {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		View view = (View) context.getBean("bothviews");

				System.out.println(view.showUsers());
		System.out.println(ClassData.getClassDataWithName("objects.User"));
	}

	}


