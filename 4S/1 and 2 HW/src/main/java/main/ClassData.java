package main;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class ClassData {
    public static String getClassDataWithName(String className)  {

        Class class2 = null;
        try {
            class2 = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String results = "";

        for (Method m : class2.getDeclaredMethods()) {
            for (Parameter p : m.getParameters()) {
                results+= wrapByTd(p.getName() + " " + p.getType());
            }
        }

        for (Field m : class2.getFields()) {
            results+= wrapByTd(m.getName() + " " + m.getType());
        }
        return results;

    }

    private static String wrapByTd(String x) {
        return "<td>" + x + "</td>";
    }
}

