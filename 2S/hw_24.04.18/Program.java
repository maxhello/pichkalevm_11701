import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

class Copier2000  implements Runnable
{
    private static final Semaphore SEMAPHORE = new Semaphore(5, true);
    String fileName;
    String dir;
    String text;
    @Override
    public void run()	//Этот метод будет выполнен в побочном потоке
    {
        try {
            SEMAPHORE.acquire();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            readFile();
            WriteFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SEMAPHORE.release();
    }
    Copier2000(String fileName, String dir){
        this.fileName = fileName;
        this.dir = dir;
    }

    public void readFile() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(dir+fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            text = everything;
        } finally {
            br.close();
        }
        System.out.println(text);
    }
    public void WriteFile() throws IOException {
//        FileWriter newJsp = new FileWriter("C:\\user\Desktop\dir1\dir2\filename.txt");
        try(FileWriter writer = new FileWriter("src/"+fileName+ "CopiedVersion"))
        {
            // запись всей строки
            writer.write(text);
            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }
}
      class CopierManager2000{
    public void copy(List<String> files, String dir){

        for (String x : files) {

            new Thread(new Copier2000(x, dir)).start();
            // System.out.println(x);
            System.out.println(x + " is being copied");
            // db.downloadUsingStream(x);
        }
    }
}


public class Program {

    public static void main(String[] args)
    {
    CopierManager2000 copierManager2000 = new CopierManager2000();
    List<String> list = new ArrayList<>();
    list.add("file1");
    list.add("file2");
    copierManager2000.copy(list, "src/");
    }

}