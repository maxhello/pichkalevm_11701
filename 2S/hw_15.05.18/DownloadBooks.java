import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.Semaphore;

public class DownloadBooks implements Runnable{
	private static final Semaphore SEMAPHORE = new Semaphore(5, true);
	
	
	
	String url;
	CopyBook2 cb;
	String nameOfSavedFile;
	
	public static int num= 0; 
	DownloadBooks(String str, CopyBook2 cb){
		url = str;
		this.cb = cb;
	}
	
	public static String file = "someFile" + num;

//	public static void downloadUsingStream(String urlStr) throws IOException {
//		URL url = new URL(urlStr);
//		BufferedInputStream bis = new BufferedInputStream(url.openStream());
//		FileOutputStream fis = new FileOutputStream("C:/Users/Макс/Desktop/algoritm");
//		byte[] buffer = new byte[1024];
//		int count = 0;
//		while ((count = bis.read(buffer, 0, 1024)) != -1) {
//			fis.write(buffer, 0, count);
//		}
//		fis.close();
//		bis.close();
//	}
	@Override
	public void run() {
		try {
			SEMAPHORE.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getUrl(url);
		System.out.println(num++ +" get back taken thread ");
		
		
		SEMAPHORE.release();
	}
	public  void getUrl(String uurl) {      // download files
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			URL url = new URL(uurl);
			bis = new BufferedInputStream(url.openStream());
			bos = new BufferedOutputStream(new FileOutputStream(new File(file)));

			int c;
			while ((c = bis.read()) != -1) { // save file
				bos.write(c);
			}
			
//			bos = new BufferedOutputStream(new FileOutputStream(new File(file+num))); 
			System.out.println("someFile" + num +  " OK");
			
//			wait(); //   wait saver process

		} catch (Exception e) {
			System.out.println(uurl + " NOT");
		} finally {
	
			try {
				bos.close();
				bis.close();
			} catch (IOException ignore) {
			}
		}
	}
}


//   class 2CopyBook extends Thread {
//	   
//	   DownloadBooks db;
//	   CopyBook(DownloadBooks db){
//		   this.db = db;
//	   }
//	  
//	   public  synchronized void copyFile() throws InterruptedException {
//		   
//		   while(true) {
//			   wait();
//		 
////		   BufferedInputStream bis = null;
//			BufferedOutputStream bos = null;
//			BufferedReader r1;
//			try {
//
//	
//				bos = new BufferedOutputStream(new FileOutputStream(new File(DownloadBooks.file+DownloadBooks.num)));
//				
//				r1 = new BufferedReader(new InputStreamReader(new FileInputStream("CopySomeFile"+ DownloadBooks.num)));
//				int c;
//				while ((c = r1.read()) != -1) {
//					bos.write(c);
//				}
//				
//				System.out.println(DownloadBooks.file+DownloadBooks.num + " OK");
//				notify();
//	   }catch (Exception e) {}
//	   }
//	   
//	   }
//		public void run() {
//		try {
//			copyFile();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		}
  
