package flappybird;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ControllerResults {
    @FXML
    public void toMenu(ActionEvent event) throws IOException {
        Parent page1 = FXMLLoader.load(getClass().getResource("../flappybird/FlappyGameMenu.fxml"));
        Scene tableScene = new Scene(page1);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableScene);
        window.show();
    }

    @FXML
    private Label results;
    @FXML
    private Button showRes;

    @FXML
    private void showResults() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/flappyScore.txt")));
        String line = br.readLine();
        int k = 0;
        while (k != 5 && line != null) {
            this.results.setText(this.results.getText() + "\n" + line);
            k++;
            line = br.readLine();
        }
        this.showRes.setVisible(false);
    }
}
