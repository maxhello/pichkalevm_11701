package results;

public class ResultGame {
    private static String GameName;
    private static String Path;
    private static String MenuPath;

    public static String getMenuPath() {
        return MenuPath;
    }

    public static void setMenuPath(String menuPath) {
        MenuPath = menuPath;
    }

    public static String getGameName() {
        return GameName;
    }

    public static void setGameName(String gameName) {
        GameName = gameName;
    }

    public static String getPath() {
        return Path;
    }

    public static void setPath(String path) {
        Path = path;
    }
}
