package results;

import connection.StateOfauthentication;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;

public class ControllerResults implements Initializable {
    @FXML
    public void toMenu(ActionEvent event) throws IOException {
        Parent page1 = FXMLLoader.load(getClass().getResource(ResultGame.getMenuPath()));
        Scene tableScene = new Scene(page1);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(tableScene);
        window.show();
    }

    @FXML
    private Label results;
    @FXML
    private Button showRes;
    @FXML
    private AnchorPane root;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            showResults();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void showResults() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("src/"+ResultGame.getPath()+".txt")));
        String line = br.readLine();
        int k = 0;
        //----------------Graphics
        CategoryAxis xAxis    = new CategoryAxis();
        xAxis.setLabel("Data");
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Score");
        BarChart barChart = new BarChart(xAxis, yAxis);
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName(ResultGame.getGameName());
        /*
        dataSeries1.getData().add(new XYChart.Data("May", 1567));
        dataSeries1.getData().add(new XYChart.Data("Sep"  , 65));
        dataSeries1.getData().add(new XYChart.Data("Oct"  , 23));
        dataSeries1.getData().add(new XYChart.Data("Dec"  , 230));
*/
        //-----------------End
        LinkedList<String> ll = new LinkedList<>();
        while (line != null) {
            ll.add(line);
            k++;
            if(k>9){
                ll.remove(0);
            }
            line = br.readLine();
        }
        k=0;
        for(String x: ll) {
            k++;
            dataSeries1.getData().add(new XYChart.Data(""+k, Integer.parseInt(x)));
        }

        barChart.getData().add(dataSeries1);
        VBox vbox = new VBox(barChart);
        vbox.setTranslateX(100);
        root.getChildren().add(vbox);
        this.showRes.setVisible(false);
    }
}
