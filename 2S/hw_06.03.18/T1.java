import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class T1 {

    public static void main(String[] args){

        Scanner sc = null;
        try {
            sc = new Scanner(new File("src\\txt.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        HashMap<Character, Integer> hm = new HashMap<>();
        while (sc.hasNext()) {
            Character c = sc.next().charAt(0);
            if (hm.containsKey(c)) {
                int count = hm.get(c);
                count++;
                hm.put(c, count);
            } else {
                hm.put(c, 1);
            }
        }
        Set<Map.Entry<Character, Integer>> set = hm.entrySet();
        System.out.println(Arrays.toString(set.toArray()));
    }
}