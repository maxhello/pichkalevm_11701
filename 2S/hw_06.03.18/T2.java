import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class T2 {

    public static void main(String[] args) {

        Scanner sc = null;
        try {
            sc = new Scanner(new File("src\\englishtext.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        HashMap<String, Integer> hm = new HashMap<>();
        String[] words = new String[0];
        while (sc.hasNext()) {
            words = sc.nextLine().split("(\" )|(\\' )|(\\. )|(\\? )|(\\, )|(\\! )|( )");
        }
        for (String x : words) {
            if (hm.containsKey(x)) {
                int count = hm.get(x);
                count++;
                hm.put(x, count);
            } else {
                hm.put(x, 1);
            }
        }

        Set<Map.Entry<String, Integer>> set = hm.entrySet();
        System.out.println(Arrays.toString(set.toArray()));
    }

    public void readTXT(){

    }
}