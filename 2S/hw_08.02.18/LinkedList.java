public class LinkedList {
    public Node getHead() {
        return head;
    }

    private Node head;
    public void append(String data){
        if (head == null) {
            head = new Node(data);
            return;
        }
        Node current = head;
        while (current.next != null){
            current = current.next;
        }
        current.next = new Node(data);
    }

    public void prepend(String data){
        Node newHead = new Node(data);
        newHead.next = head;
        head = newHead;
    }

    public void deleteWithValue(String data) {
        if(head == null) return;
        if(head.data == data) {
            head = head.next;
            return;
        }

        Node current = head;
        while (current.next != null) {
            if(current.next.data == data) {
                current.next = current.next.next;
                return;
            }
            current = current.next;
        }
    }
    public void printLinkedList(){

        Node current = head;
        if(current == null) return;
        else System.out.println(current.getData());

        while(current.next != null){
            current = current.next;
            System.out.println(current.getData());
        }
    }
    public void concantenationWithAnotherOne(LinkedList another){
      Node anotherHead = another.getHead();
      Node current = head;
      if(current == null) return;
        while(current.next != null){
            current = current.next;

        }
        current.next=anotherHead;
    }
}

    class Node {
    Node next;

        public String getData() {
            return data;
        }

        String data;
    public Node(String data){
        this.data=data;
    }


}