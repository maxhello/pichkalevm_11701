package Tasks;

import entities.Carmakers;
import entities.Continent;
import entities.Country;

import java.util.List;

public class LoadAllLists {
    public static List<Continent> contList;
    public static List<Country> countryList;
    public static List<Carmakers> makersList;


    LoadAllLists(){
        Continent cont = new Continent();
        contList = cont.takeList();
        Country country = new Country();
        countryList = country.takeList();
        Carmakers carmakers = new Carmakers();
        makersList = carmakers.takeList();
    }
}
