package Tasks;

import entities.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        LoadAllLists loadAllLists = new LoadAllLists();
        List<CarNames> list =  loadAllLists.carNamesList.stream().filter((p)-> "1970".equals(p.getCarsData().getYear())).distinct().collect(Collectors.toList());
        list.stream().filter(distinctByKey(p-> p.getModel().getMaker().getMaker())).peek(t-> System.out.println(t.getModel().getMaker().getMaker())).collect(Collectors.toList());
    }
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
