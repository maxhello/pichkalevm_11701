package Tasks;

import entities.Continent;
import entities.Country;

import java.util.*;

import java.util.Map;
import java.util.stream.Collectors;

public class Task1 {

        public static void main(String[] args) {
            LoadAllLists loadAllLists = new LoadAllLists();
            Country cy = new Country();
            List ar = cy.takeList();
            Continent ct = new Continent();

            List arOfCont = ct.takeList();

            Map<Integer, List<Country>> map = (Map<Integer, List<Country>>) ar.stream().collect(Collectors.groupingBy(Country::getContinent));
            System.out.println(map);
            int k = 0;
            String continent = null;
            for (Map.Entry<Integer, List<Country>> entry : map.entrySet()) {
                 if(entry.getValue().size() > k){
                     k = entry.getValue().size();
                     continent = entry.getValue().get(0).getContinent().getId();
                 }
            }
        }
}
