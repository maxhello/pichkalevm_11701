package Tasks;

import entities.Carmakers;
import entities.Country;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task3 {
    public static void main(String[] args) {
        LoadAllLists loadAllLists = new LoadAllLists();
        Map<Country, List<Carmakers>> map = (Map<Country, List<Carmakers>>) loadAllLists.makersList.stream().collect(Collectors.groupingBy(Carmakers::getCountry));
        for(Map.Entry<Country, List<Carmakers>> x: map.entrySet()){
            System.out.println(x.getKey().getName() +" "+x.getValue().size());
        }
    }
}
