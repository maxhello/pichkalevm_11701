package collection_singletons;

import java.util.List;

public interface ReadableToList <T>{
    public T createMyself();
    public List takeList();
}
