package collection_singletons;

import entities.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public abstract class Fabric implements ReadableToList {
    private static String filename = "res/countries.csv";
    private static String filenameCont = "res/continents.csv";
    private static String filenameMakers = "res/car-makers.csv";
    private static String filenameCarsNames = "res/car-names.csv";
    private static String filenameCars = "res/cars.csv";
    private static String filenameCarsData = "res/cars-data.csv";
    private static String filenameModelList = "res/model-list.csv";
    private  List ListToReturn;
    public String[] attrs;

    public List getSomeStructure(String filename) {
//        switch (str){
//            case "countries":
//                string = "res/countries.csv";
//            case "continents":
//                string = "res/continents.csv";
//
//            case "car-makers":
//                string = "res/car-makers.csv";
//            case "car-names":
//                string = "res/car-names.csv";
//            case   "cars":
//                string = "res/cars.csv";
//            case    "cars-data":
//                string =  "res/cars-data.csv";
//            case    "model-list":
//                string = "res/model-list.csv";
//
//        }

        if (ListToReturn == null) {
            ListToReturn = new ArrayList<>();
            try {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(filename)
                        )
                );
                String line = br.readLine();
                int k = 0;
                while (line != null) {
                    if(k != 0){

                        attrs = line.split(",");
                        ListToReturn.add(createMyself());
                        line = br.readLine();
                    }else{
                        k++;
                        line = br.readLine();
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(ListToReturn.size());
        return ListToReturn;
    }


//    public Carmakers createCarMaker(){
//        return new Carmakers(attrs[0], attrs[1], attrs[2], attrs[3]);
//
//    }
//    public CarNames createCarNames(){
//        return new CarNames(attrs[0], attrs[1], attrs[2]);
//    }
//    public CarsData createCarData(){
//        return new CarsData(attrs[0], attrs[1], attrs[2], attrs[3], attrs[4], attrs[5], attrs[6], attrs[7]);
//    }
//    public Continent createContinent(){
//        return new Continent(attrs[0], attrs[1]);
//    }
//    public ModelList createModelList(){
//        return new ModelList(attrs[0], attrs[1], attrs[2]);
//    }
//    public Country createCountry(){
//        return new Country(attrs[1], attrs[0], Integer.parseInt(attrs[2]));
//    }


//    @Override
//    public Object createMyself() {
//        return null;
//    }
}
