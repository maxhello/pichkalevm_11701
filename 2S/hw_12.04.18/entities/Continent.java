package entities;

import collection_singletons.Fabric;

import java.util.List;

public class Continent extends Fabric {
    String id;
    private static String filenameCont = "res/continents.csv";

    public List takeList(){
        return getSomeStructure(filenameCont);
    }

        public Continent createMyself(){
        return new Continent(attrs[0], attrs[1]);
    }

    public Continent(){

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;


    public Continent(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Continent{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
