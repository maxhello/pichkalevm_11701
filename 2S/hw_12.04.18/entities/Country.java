package entities;

import collection_singletons.Fabric;

import java.util.List;

import static Tasks.LoadAllLists.contList;

public class Country extends Fabric {

     String name;
    String id;
    Continent continent;
    private static String filename = "res/countries.csv";
    public List takeList(){
        return getSomeStructure(filename);
    }


        public Country createMyself(){
        Continent continent = null;
            for(int i = 0; i<contList.size(); i++){
                if(contList.get(i).getId().equals(attrs[2])){
                    continent = contList.get(i);
                }
            }
            return new Country(attrs[1], attrs[0], continent);
    }

    public Country(){

    }

    public Country(String name, String id, Continent continent) {
        this.name = name;
        this.id = id;
        this.continent = continent;

    }

    public void setId(String id) {
        this.id = id;
    }

    public  String getName() {
        return name;
    }
    public  String getId() {
        return id;
    }


    public  Continent getContinent() {
        return continent;
    }


    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", continent=" + continent +
                '}';
    }


}
