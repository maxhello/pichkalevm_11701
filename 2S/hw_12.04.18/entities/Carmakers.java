package entities;

import collection_singletons.Fabric;

import java.util.List;

import static Tasks.LoadAllLists.countryList;

public class Carmakers extends Fabric {
    String id;
    String maker;
    String fullName;
    Country country;
    private static String filenameMakers = "res/car-makers.csv";
    public List takeList(){
        return getSomeStructure(filenameMakers);
    }
    public String getId() {
        return id;
    }
    public Carmakers(){

    }

    @Override
    public String toString() {
        return "Carmakers{" +
                "id='" + id + '\'' +
                ", maker='" + maker + '\'' +
                ", fullName='" + fullName + '\'' +
                ", country=" + country.getName() +
                '}';
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Carmakers(String id, String maker, String fullName, Country country) {

        this.id = id;
        this.maker = maker;
        this.fullName = fullName;
        this.country = country;
    }

    @Override
    public Object createMyself() {
        Country country = null;
        for(int i= 0; i< countryList.size(); i++){
            if(countryList.get(i).getId().equals(attrs[3]))
                country = countryList.get(i);
//            System.out.println(1);
        }
        return new Carmakers(attrs[0], attrs[1], attrs[2], country);

    }
}
