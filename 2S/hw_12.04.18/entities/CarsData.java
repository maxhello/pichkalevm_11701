package entities;

import collection_singletons.Fabric;

import java.util.List;

public class CarsData extends Fabric {
    String id;
    String mPG;
    String cylinders;
    String edispl;
    String horsepower;
    String weight;
    String accelerate;
    private static String filenameCarsData = "res/cars-data.csv";
    public List takeList(){
        return getSomeStructure(filenameCarsData);
    }

        public CarsData createMyself(){
        return new CarsData(attrs[0], attrs[1], attrs[2], attrs[3], attrs[4], attrs[5], attrs[6], attrs[7]);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmPG() {
        return mPG;
    }

    public void setmPG(String mPG) {
        this.mPG = mPG;
    }

    public String getCylinders() {
        return cylinders;
    }

    public void setCylinders(String cylinders) {
        this.cylinders = cylinders;
    }

    public String getEdispl() {
        return edispl;
    }

    public void setEdispl(String edispl) {
        this.edispl = edispl;
    }

    public String getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(String horsepower) {
        this.horsepower = horsepower;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(String accelerate) {
        this.accelerate = accelerate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public CarsData(String id, String mPG, String cylinders, String edispl, String horsepower, String weight, String accelerate, String year) {

        this.id = id;
        this.mPG = mPG;
        this.cylinders = cylinders;
        this.edispl = edispl;
        this.horsepower = horsepower;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }

    String year;
}
