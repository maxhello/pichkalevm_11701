package entities;

import collection_singletons.Fabric;

import java.util.List;

public class CarNames extends Fabric {
    String id;
    String model;
    String make;
    private static String filenameCarsNames = "res/car-names.csv";
    public List takeList(){
        return getSomeStructure(filenameCarsNames);
    }

        public CarNames createMyself(){
        return new CarNames(attrs[0], attrs[1], attrs[2]);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public CarNames(String id, String model, String make) {

        this.id = id;
        this.model = model;
        this.make = make;
    }
}
