package entities;

import collection_singletons.Fabric;

import java.util.List;

import static Tasks.LoadAllLists.makersList;

public class ModelList extends Fabric {
    String modelId;
    Carmakers maker;
    private static String filenameModelList = "res/model-list.csv";
    public List takeList(){
        return getSomeStructure(filenameModelList);
    }

        public ModelList createMyself(){
        Carmakers carmakers = null;
        for(int i = 0; i < makersList.size(); i++){
            if(makersList.get(i).equals(attrs[1]))
                carmakers =makersList.get(i);
        }
        return new ModelList(attrs[0], carmakers, attrs[2]);
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public Carmakers getMaker() {
        return maker;
    }

    public void setMaker(Carmakers maker) {
        this.maker = maker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public ModelList(String modelId, Carmakers maker, String model) {


        this.modelId = modelId;
        this.maker = maker;
        this.model = model;
    }

    String model;
}
