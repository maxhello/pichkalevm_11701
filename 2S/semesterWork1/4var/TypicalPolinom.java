public interface TypicalPolinom {
    String toString();
     void insert(int coef, int deg);
    void combine();
    void delete(int deg);
    void sum(Polinom p);
    void derivate();
    int value(int x);
    void deleteOdd();

}
