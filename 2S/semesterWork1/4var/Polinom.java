import java.io.*;
import java.util.Scanner;

public class Polinom  {
    public  IntArrayCollection arr = new IntArrayCollection();
    private int degree = 0;

    public int getDegree(){
        return  degree;
    }
    Polinom(String data)  {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(data));
        } catch (FileNotFoundException e) {

        }

        int count = 0;
        int coef = 0;
        int currentValue;
        while (scanner.hasNext() == true) {

            currentValue = Integer.parseInt(scanner.nextLine());
            if (count == 2) count = 0;
            count++;
            if (count == 1) coef = currentValue;
            else {
                insert(coef, currentValue);
                if(isDegreeLess(degree, currentValue))degree = currentValue;
            }

        }
    }
    public int getValue(int num){
        return arr.get(num);
    }
    public String toString(){
        StringBuilder s = new StringBuilder("");
        for (int i = degree; i >= 0; i--) {
            if (getValue(i) == 0) continue;
            if ((getValue(i) > 0) && (i != degree))
                s.append("+");
            s.append(getValue(i));
            if (i == 1) s.append("x");
            else if (i != 0) s.append("x^" + i);
        }
        return s.toString();
    }

    public  boolean isDegreeLess(int degree, int deg){
        return degree<deg;
    }
    public  void insert(int coef, int deg) {
        if(isDegreeLess(degree, deg))degree = deg;
        arr.addAtDegPosition(coef, deg);
    }

@Deprecated
   public void combine() {

    }


    public void delete(int deg) {
        if(deg == degree)
            for(int i = degree-1; i >= 0; i--){
            if(arr.get(i) != 0){
                degree = i;
                break;
            }
                if(i == 0)degree = 0;
            }
        arr.remove(deg);
    }


    public void sum(Polinom p) {
        int coef = 0;
        int d;
        int k = p.getDegree();
        for(int i = 0; i <= k; i++){
            coef = p.getValue(i);
            if(coef != 0) insert(coef, i);
        }
    }


    public  void derivate() {
        for(int i = 0; i <= arr.getCapacity(); i++){
            getValue(i);
            if(i == 0)arr.remove(i); else{
                arr.addAtDegPosition(i*getValue(i),i-1);
                arr.remove(i);

            }
        }
        degree--;
    }

    public  int value(int x) {
        int p = x*getValue(degree) + getValue(degree-1);
        for(int i = degree-2; i >= 0; --i){
            p = p*x + getValue(i);
        }
        return p;
    }

    public  void deleteOdd() {
        int i = 1;
        while (i <= degree){
            delete(i);
            i+=2;
        }
    }
}

