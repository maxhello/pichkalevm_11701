import java.util.*;

public class IntArrayCollection implements Collection<Integer> {
    private int CAPACITY = 4;
    private int [] arr = new int[5];
    private int n = 0;

     public int[] getArray(){
    return arr;
}
    public int getCapacity(){
        return CAPACITY;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            for (Integer x: arr) {
                if (x == i) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public int get(int num){
        return arr[num];
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        return false;
    }



    public boolean addAtDegPosition(Integer i, Integer pos) {
        if(pos > CAPACITY){
            CAPACITY = pos;
            resize(CAPACITY+1);
        }
        if(arr[pos] == 0)n++;
        arr[pos] += i;
        return true;
    }

    //ToDo
    private void resize(int newLength) {
        int[] newArray = new int[newLength+1];

//        System.arraycopy(arr, 0, newArray, 0, n);
        for(int i = 0; i< arr.length; i++){
            newArray[i]= arr[i];
        }
        arr = newArray;
    }


    @Override
    public boolean remove(Object o) {
        Integer i = null;
        if(o instanceof Integer)
            i =(Integer)o;
        arr[i] = 0;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        boolean pr = false;
        for (Integer i: c) {
            pr = this.add(i) || pr;
        }
        return pr;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}