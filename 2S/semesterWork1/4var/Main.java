import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

         ///!!!!!!!! Standard is 3x^3+2x^2+x^1 !!!!!!!\\\\\\\\
        Polinom pol = new Polinom("src/data");
        System.out.println(pol.getValue(1));// constructor
        System.out.println(pol.getValue(2));// constructor
        System.out.println("constructor "+ pol.getValue( 3));// constructor

        System.out.println("toString "+ pol); //toString
        pol.insert(4,5);//insert
        System.out.println("insert " + pol.getValue(5));//insert
        System.out.println("insert "+ pol); //insert
        pol.delete(5);// deleting
        System.out.println("delete " +pol); //delete
        System.out.println("value " + pol.value(2));//value is 34
        pol.derivate();// derivating
        System.out.println("derivate " + pol.toString());// derivating
        Polinom pol2 = new Polinom("src/data");// sum 3x^3+2x^2+x^1 +++ 3x^3+2x^2+x^1
        Polinom pol3 = new Polinom("src/data"); //sum
        pol2.sum(pol3); // sum     !! must be 6x^3+4x^2+2x t is proved!
        pol3.deleteOdd();//   1x
        System.out.println("than odd numbers w a s deleted "+ pol3);//  2x^2
        System.out.println("HEEEEEY!!");
        Polinom pol9 = new Polinom("src/data");// sum 3x^3+2x^2+x^1 +++ 10x^20+2x^2+x^1
        Polinom pol8 = new Polinom("src/polinom2"); //sum
        pol8.sum(pol9);
        pol9.sum(pol8);
        System.out.println(pol8.toString());
        System.out.println(pol9.toString());
    }
}
