import org.junit.Assert;
import org.junit.Test;
public class CollectionTest {

    @Test
    public void constructorTest(){

        Polinom pol = new Polinom("src/data");
        Assert.assertEquals(3, pol.getValue(3));
        Assert.assertEquals(1, pol.getValue(1));
        Assert.assertEquals(2, pol.getValue(2));
    }


    @Test
    public void InsertTest() {
        int [] expected = new int[5];
        expected[1]=2;
        expected[2]=4;
        expected[3]=6;

        Polinom pol = new Polinom("src/data");
        pol.insert(1,1);
        pol.insert(2,2);
        pol.insert(3,3);
        Assert.assertArrayEquals(expected, pol.arr.getArray());

    }

    @org.junit.Test
    public void toStringTest() {
        Polinom pol = new Polinom("src/data");
        Assert.assertEquals("3x^3+2x^2+1x", pol.toString());
    }

    @org.junit.Test
    public void deleteTest() {
        Polinom pol = new Polinom("src/data");
        pol.delete(1);
        Assert.assertEquals(0, pol.getValue(1));
    }


    @org.junit.Test
    public void deleteOddTest() {
        Polinom pol = new Polinom("src/data");
        pol.deleteOdd();
        Assert.assertEquals(0, pol.getValue(1));
        Assert.assertEquals(2, pol.getValue(2));
        Assert.assertEquals(0, pol.getValue(3));
    }

    @org.junit.Test
    public void valueTest() {
        Polinom pol = new Polinom("src/data");
        Assert.assertEquals(34,  pol.value(2));

    }

    @org.junit.Test
    public void derivateTest() {
        Polinom pol = new Polinom("src/data");
        pol.derivate();
        Assert.assertEquals("9x^2+4x+1",  pol.toString());
    }

    @org.junit.Test
    public void sumTest() {
        Polinom pol = new Polinom("src/polinom2");
        Polinom pol2 = new Polinom("src/data");
        pol.sum(pol2);
        Assert.assertEquals("10x^20+3x^3+4x^2+2x",  pol.toString());
    }
}