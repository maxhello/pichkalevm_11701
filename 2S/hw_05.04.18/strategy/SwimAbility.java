package strategy;

public interface SwimAbility {
    public  void swim();
}
