package strategy;

public class Main {
    public static void main(String[] args) {
        SomeMechanism car = new Car();
        car.setDriveAbility(new AggressiveDriving());
        car.drive();
    }
}
