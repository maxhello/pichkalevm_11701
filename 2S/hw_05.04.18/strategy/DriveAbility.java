package strategy;

public interface DriveAbility {
    public void drive();
}
