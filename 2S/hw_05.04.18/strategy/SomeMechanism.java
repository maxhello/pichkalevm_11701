package strategy;

public abstract class SomeMechanism {
    SwimAbility swimAbility;
    DriveAbility driveAbility;


    public void setDriveAbility(DriveAbility driveAbility) {
        this.driveAbility = driveAbility;
    }

    public void setSwimAbility(SwimAbility swimAbility) {

        this.swimAbility = swimAbility;
    }
    public void swim() {
        swimAbility.swim();
    }

    public void drive() {
        driveAbility.drive();
    }
}
