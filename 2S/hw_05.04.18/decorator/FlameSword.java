package decorator;

public class FlameSword extends Increasing {

    Weapon weapon;

    public FlameSword(Weapon weapon){
        this.weapon = weapon;
    }

    @Override
    public String getAttack() {
        return weapon.getAttack() + " with FIREEEEEEE!!!!!";
    }
}
