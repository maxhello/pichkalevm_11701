package decorator;

public class Sword extends Weapon {

    String ability = "fight with sword";
    public String getAttack() {
        return ability;
    }
}
