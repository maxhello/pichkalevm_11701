package decorator;

public class StartDecorate {
    public static void main(String[] args) {
        Weapon swordWeapon = new Sword();
        System.out.println(swordWeapon.getAttack());
        swordWeapon = new FlameSword(swordWeapon);
        System.out.println(swordWeapon.getAttack());

    }
}
