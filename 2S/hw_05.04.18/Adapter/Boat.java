package Adapter;

public interface Boat {
    public void startBoatEngine();
}
