package Adapter;

import java.util.Map;

public class AdapterToCar implements Boat {
    Car car;

    AdapterToCar(Car car){
    this.car = car;
    }

    @Override
    public void startBoatEngine() {
    car.startCarEngine();
    }
}
