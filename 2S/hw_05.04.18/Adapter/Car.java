package Adapter;

public interface Car {
    void startCarEngine();
}
