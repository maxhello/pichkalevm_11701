package Singleton;

public class SingletonSun {
    SingletonSun refference;
    String heat = "very hot";
    SingletonSun(){
    }

    public SingletonSun getRefference() {
        if(refference == null)
            refference = new SingletonSun();

        return refference;
    }
}
