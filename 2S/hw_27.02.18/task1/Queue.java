package task1;

import java.util.NoSuchElementException;

public class Queue<T> {
    Node head; // last element
    Node last;
    public void add(T value){
        Node node = new Node(value);
        if(head == null){
            head = node;
            last = node;
        } else {
            last.previous = node;
        }
    }

    public T peek(){
        if(head != null)
        return (T)head.getValue();
        else return null;
    }

    public T element(){
        if(head != null)
            return (T)head.getValue();
        else throw new NoSuchElementException(" queue is empty");
    }
    public void remove(){
        if(head != null)
            head = head.getPrevious();
        else throw new NoSuchElementException(" queue is empty");
    }
    public T pull(){
        if(head != null){
            T t  = (T)head.getValue();
            head = head.getPrevious();
            return t;
        }
        else return null;
    }

}
