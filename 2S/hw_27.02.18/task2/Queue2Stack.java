package task2;

import java.util.NoSuchElementException;

public class Queue2Stack<T> {
 Stack stack1 = new Stack();
 Stack stack2 = new Stack();

    public void add(T value){
        stack1.push(value);
    }

    private void fillStack2(){
        while(stack1.head != null){
            stack2.push(stack1.pop());
        }
    }

    public T peek(){
        fillStack2();
        if(stack2.head == null){
            return null;
        } else
        return (T) stack2.peek();
    }


    public T element(){
        if(stack2.head != null)
            return (T)stack2.peek();
        else throw new NoSuchElementException(" queue is empty");
    }
    public void remove(){
        if(stack2.head != null)
            stack2.pop();
        else throw new NoSuchElementException(" queue is empty");
    }
    public T pull(){
        if(stack2.head != null){
            T t  = (T)stack2.pop();
            return t;
        }
        else return null;
    }

}
