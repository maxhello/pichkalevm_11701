package task2;

import java.util.EmptyStackException;

public class Stack <T> {
    Node head;
    public void push(T value){
        Node node = new Node(value);

        if(head == null){
        head = node;
        }else{
            node.setPrevious(head);
            head = node;
        }

        }


        public T pop(){
        if(head != null){

            T t = (T)head.getValue();
            head = head.previous;
            return t;
        } else {
            throw new EmptyStackException();
        }
        }

    public T peek(){
        if(head != null){
            return (T)head.getValue();
        } else {
            throw new EmptyStackException();
        }
    }

    // another methods are unnecessary

}
