// Radix sort Java implementation
import java.io.*;
import java.util.*;

public class RadixSortArrayList {

    public int [] negativeArray;
    public int [] positiveArray;

    static int getMax(int arr[], int n)
    {
        int mx = arr[0];
        for (int i = 1; i < n; i++)
            if (arr[i] > mx)
                mx = arr[i];
        return mx;
    }
    static int getNumber(int num, int exp){


        return (num/exp)%10;
    }


    static void countSort(int arr[], int n, int exp)
    {
        int output[] = new int[n]; // output array
        int i;
        int count[] = new int[10];
        Arrays.fill(count,0);


        //count
        for (i = 0; i < n; i++)
            count[getNumber(arr[i],exp)]++;
        // order
        for (i = 1; i < 10; i++)
            count[i] += count[i - 1];

        // algorithm that afford us
        for (i = n - 1; i >= 0; i--)
        {
            output[count[getNumber(arr[i],exp)] - 1] = arr[i];
            count[getNumber(arr[i],exp)]--;
        }

// rebult order
        for (i = 0; i < n; i++)
            arr[i] = output[i];
    }

    static void radixsort(int arr[], int n)

    {
        // max
        int m = getMax(arr, n);

        //
        for (int exp = 1; m/exp > 0; exp *= 10)
            countSort(arr, n, exp);
    }

    // print
    static void print(int arr[], int n)
    {
        for (int i=0; i<n; i++)
            System.out.print(arr[i]+" ");
    }

    public void negativeToPositive(int [] arr){
        for(int i = 0; i < arr.length; i++){
            arr[i] *= -1;

        }
    }

    public void fillNegativeAndPositiveArrays(ArrayList arr ){
        int k_negative = 0;
        int k_positive = 0;
        for(int i = 0; i < arr.size(); i ++){

            if(arr.get(i) < 0){
                negativeArray[k_negative] = arr.get(i);
                k_negative++;
            }

            if(arr.get(i) >= 0){
                positiveArray[k_positive] = arr.get(i);
                k_positive++;
            }
        }
    }

    public int howMuchPositiveAndNegative(ArrayList arr){
        int negative_C = 0;

        for(int i = 0 ; i < arr.size(); i++){
            if(arr.get(i) < 0) negative_C++;
        }
        return negative_C;
    }

    public void startForArrayList(ArrayList arr){
        int neg_c = howMuchPositiveAndNegative(arr);
        negativeArray = new int [neg_c];
        positiveArray = new int [arr.size()-neg_c];
        fillNegativeAndPositiveArrays(arr);
        if(negativeArray.length == 0){
            radixsort(positiveArray, positiveArray.length);
            fillMainArray(arr);
        } else
        {
            negativeToPositive(negativeArray);
            radixsort(negativeArray, negativeArray.length);
            radixsort(positiveArray, positiveArray.length);
            turnOver(negativeArray);
            negativeToPositive(negativeArray);
            fillMainArray(arr);
        }

//        System.out.println(Arrays.toString(arr));
    }

    public void fillMainArray(ArrayList arr){
        for(int i = 0; i < negativeArray.length; i ++){
            arr.add(i, negativeArray[i]);

        }
        for(int i = 0; i < positiveArray.length; i ++){
            arr.add(i+negativeArray.length, positiveArray[i]);
        }
    }

    public void turnOver(int[] massive) {
        for (int i = 0; i < massive.length / 2; i++) {
            int tmp = massive[i];
            massive[i] = massive[massive.length - i - 1];
            massive[massive.length - i - 1] = tmp;
        }
    }

//    public static void main (String[] args)
//    {
//        int arr[] = {170, 45, 75, 90, 802, 24, 2, 66};
//        int n = arr.length;
//        radixsort(arr, n);
//        print(arr, n);
//    }
}