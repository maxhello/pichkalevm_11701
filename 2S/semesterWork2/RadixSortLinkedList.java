import java.util.LinkedList;
import java.util.Random;

public class RadixSortLinkedList {
private   LinkedListR positiveLL ;
private  LinkedListR negativeLL ;
    private  LinkedListR[] q = {
            new LinkedListR(), // 0
            new LinkedListR(), // 1
            new LinkedListR(), // 2
            new LinkedListR(), // 3
            new LinkedListR(), // 4
            new LinkedListR(), // 5
            new LinkedListR(), // 6
            new LinkedListR(), // 7
            new LinkedListR(), // 8
            new LinkedListR() // 9
    };

        // print result
        public void radixSort(LinkedListR radL){
            int kMax = maxV(radL);
            int div = 1;
            int divRest = 10;
            while (kMax >= divRest) {


                Node current = radL.getHead();
                int curInt;
                while (current != null) {          // добавляю числа по местам относительно запрашиваемой цифры
                    curInt = current.getData() % divRest / div;
//                    System.out.println(curInt);
                    q[curInt].add(current.getData());
                    current = current.next;
                }

                div *= 10;
                divRest *= 10;
                boolean flag = false;
                for (int i = 0; i < q.length; i++) {      // создаю линкед лист по порядку из других девяти линкед листов
                    if (q[i].getHead() != null) {         //

                        Node currentN = q[i].getHead();
//                        System.out.println(currentN.getData());
                        if (!flag) {
                            flag = true;
                            radL.putHead(currentN);
                        } else {
                            radL.addAll(q[i]);
                        }
                    }
                }
//                        Node currents = radL.getHead();     print loops
//            while(currents != null){
//                System.out.println(currents.getData());
//                currents = currents.next;
//            }
//                System.out.println("FIN");

                for(int i = 0; i < q.length; i++){        // обнуляю девять линкедлистов
                    q[i].putHead(null);
                }
        }
    }

     public void fillNegativeAndPositiveLinkedList(LinkedListR ll){
            Node cur = ll.getHead();
            while(cur != null){
                if(cur.getData() < 0)negativeLL.add(cur.getData());
                if(cur.getData() >= 0)positiveLL.add(cur.getData());
                cur = cur.next;
            }

     }

     public void negativeToPositive(LinkedListR ll){
            Node cur = ll.getHead();
            while(cur != null){
                cur.data = cur.getData()*(-1);
                cur = cur.next;
            }

     }

     public void turnOver(LinkedListR ll){        // turnover linkedList (for negative)
        Node cur = ll.getHead();
        int [] ar = new int[ll.size()];

        for(int i = ar.length-1; i >= 0; i-- ){
            ar[i] = cur.getData();
            cur = cur.next;
        }

        cur = ll.getHead();

        for(int i = 0; i < ar.length; i ++){
            cur.data = ar[i];
            cur = cur.next;
        }
     }

     public void sortLinkedList(LinkedListR ll){
            negativeLL = new LinkedListR();
            positiveLL = new LinkedListR();
            fillNegativeAndPositiveLinkedList(ll);
            if(negativeLL.size() == 0){
                radixSort(positiveLL);
                ll.putHead(positiveLL.getHead());
            }else{
                negativeToPositive(negativeLL);
                radixSort(negativeLL);
                radixSort(positiveLL);
                turnOver(negativeLL);
                negativeToPositive(negativeLL);
                connectLinkedLists(ll);
                negativeLL.putHead(null);
                positiveLL.putHead(null);
            }
     }

     public void connectLinkedLists (LinkedListR ll ){

            ll.putHead(negativeLL.getHead());
            ll.addAll(positiveLL);
//            ll_new.printLinkedList();
     }

    public static int maxV(LinkedListR ls){
        Node current = ls.getHead();
        int max = 0;
        int k = 10;
        while(current != null){
            if(max < current.getData()) max = current.getData();
            current = current.next;
        }
        while(max>0){
//            System.out.println(max);
//            System.out.println(k);
            k *=10;
            max = max/10;

        }
        return k;
    }
}