// Radix sort Java implementation
import java.io.*;
import java.util.*;

public class RadixSort  {


    static int getMax(int arr[], int n)
    {
        int mx = arr[0];
        for (int i = 1; i < n; i++)
            if (arr[i] > mx)
                mx = arr[i];
        return mx;
    }
    static int getNumber(int num, int exp){


        return (num/exp)%10;
    }


    static void countSort(int arr[], int n, int exp)
    {
        int output[] = new int[n]; // output array
        int i;
        int count[] = new int[10];
        Arrays.fill(count,0);


        //count
        for (i = 0; i < n; i++)
            count[getNumber(arr[i],exp)]++;
        // order
        for (i = 1; i < 10; i++)
            count[i] += count[i - 1];

        // algorithm that afford us
        for (i = n - 1; i >= 0; i--)
        {
            output[count[getNumber(arr[i],exp)] - 1] = arr[i];
            count[getNumber(arr[i],exp)]--;
        }

// rebult order
        for (i = 0; i < n; i++)
            arr[i] = output[i];
    }

    static void radixsort(int arr[], int n)

    {
        // max
        int m = getMax(arr, n);

       //
        for (int exp = 1; m/exp > 0; exp *= 10)
            countSort(arr, n, exp);
    }

    // print
    static void print(int arr[], int n)
    {
        for (int i=0; i<n; i++)
            System.out.print(arr[i]+" ");
    }
    public void startForArrayList(ArrayList arrayList){
        int n = arrayList.size();
//        int [] arr = new int[n];
//        for(int i = 0; i < n; i++){
//            arr[i] = arrayList.get(i);
//        }
        radixsort(arrayList.getArr(), n);
//        print(arrayList.getArr(), n);

    }
    public void startForArray(int [] arr){

        int n = arr.length;
        radixsort(arr, n);
//        print(arr, n);
    }

//    public static void main (String[] args)
//    {
//        int arr[] = {170, 45, 75, 90, 802, 24, 2, 66};
//        int n = arr.length;
//        radixsort(arr, n);
//        print(arr, n);
//    }
}