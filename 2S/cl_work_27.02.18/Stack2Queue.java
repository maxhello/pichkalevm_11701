public class Stack2Queue<T> {
    Stack<T> in = new Stack<>();
    Stack<T> out = new Stack<>();

    public void push(T item){
        in.push(item);
    }

    public T pop() {
        if(out.isEmpty()) {
            while (!in.isEmpty()) {
                out.push((T)in.pop().getData());
            }
        }
        return (T)out.pop().getData();
    }
}
