import java.util.Scanner;

public class ThirdTask {

    static int min = 0;
   static QueueList<Integer> queueList1 = new QueueList<>();
   static QueueList<Integer> queueList2 = new QueueList<>();
   static QueueList<Integer> queueList3 = new QueueList<>();
    static int q1_new;
    static int q2_new;
    static int q3_new;

    public void run(){
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.next());
        queueList1.push(2);
        queueList2.push(3);
        queueList3.push(5);
        int k = 0;
        int q1;
        int q2;
        int q3;
        while(n > min){
            q1 = queueList1.pick();
            q2 = queueList2.pick();
            q3 = queueList3.pick();

            if(q1<=q2 && q1<=q3){
                min = q1;
                System.out.println(min);
                getNewElem();
                queueList1.pop();
            }else if(q2<=q1 && q2 <= q3){
                min = q2;
                System.out.println(min);
                getNewElem();
                queueList2.pop();
            }else if(q3<=q1 && q3 <= q2){
                min = q3;
                System.out.println(min);
                getNewElem();
                queueList3.pop();
            }
            putN();
        }
    }
    public void getNewElem(){
         q1_new = queueList1.pick();
        q2_new = queueList2.pick();
        q3_new = queueList3.pick();
    }
    public void putN(){
        queueList1.push(q1_new*2);
        queueList2.push(q2_new*3);
        queueList3.push(q3_new*5);

    }
}
