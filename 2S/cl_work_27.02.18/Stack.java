public class Stack <T>  {

    int size;
    public Node getHead() {
        return head;
    }



    private Node head;
    public void push(T data){
        if (head == null) {
            head = new Node(data);
            size++;
            return;
        }
        Node newEl = new Node(data);
        newEl.next = head;
        head = newEl;
    }
    public Node pop() {
        Node x = head;
        head = head.next;
        return x;
    }

    public void printLinkedList(){

        Node current = head;
        if(current == null) return;
        else System.out.println(current.getData());

        while(current.next != null){
            current = current.next;
            System.out.println(current.getData());
        }
    }

//    public boolean contain(T data) {
//        if(head == null) return false;
//        if(head.data == data) {
//            return true;
//        }
//
//        Node current = head;
//        while (current.next != null) {
//            if(current.next.data == data) {
//                return true;
//            }
//        }
//        return false;
//    }

    public boolean isEmpty() {
        if( getHead() == null) return true;
        else return false;
    }
}



//class Node<T> {
//    Node next;
//
//    public  T getData() {
//        return data;
//    }
//
//    T data;
//    public Node(T data){
//        this.data=data;
//    }
//}