package Tasks;

import entities.Appellation;
import entities.Grape;
import entities.Wine;
import readingCSV.ReaderSaverOfLists;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static readingCSV.ReaderSaverOfLists.*;

public class Task1 {
    public static void main(String[] args) {
        ReaderSaverOfLists readerSaverOfLists = new ReaderSaverOfLists();
        readerSaverOfLists.startReadAndCreate();
//
//        for (Appellation w : readerSaverOfLists.appellationList)
//            System.out.println(w.getName());
//
//        for (Grape w : readerSaverOfLists.grapeList)
//            System.out.println(w.getName());


//        for(Wine w:readerSaverOfLists.wineList)
//            System.out.println(w.getGrape().getName());



        countTheWinesInAppellationsByFOR();
    }


   public static void countTheWinesInAppellationsByFOR(){
       int maxCountOfWines = 0;
       Appellation appellation = null;
       for(int i = 0; i < appellationList.size(); i++){
           if(appellationList.get(i).getListOfWines().size()> maxCountOfWines)
               maxCountOfWines = appellationList.get(i).getListOfWines().size();
           appellation = appellationList.get(i);

       }
       System.out.println(maxCountOfWines);
       System.out.println(appellation.getName());
   }

    public static void countTheWinesInAppellationsByStreamAPI(){
        int max = 100000;
        appellationList.stream().peek(z-> System.out.println(z.getListOfWines().size())).collect(Collectors.toList());

    }


}
