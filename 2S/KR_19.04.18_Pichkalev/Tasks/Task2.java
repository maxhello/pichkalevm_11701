package Tasks;

import entities.Appellation;
import entities.Grape;
import readingCSV.ReaderSaverOfLists;

import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

import static readingCSV.ReaderSaverOfLists.appellationList;
import static readingCSV.ReaderSaverOfLists.grapeList;

public class Task2 {
    public static void main(String[] args) {
        ReaderSaverOfLists readerSaverOfLists = new ReaderSaverOfLists();
        readerSaverOfLists.startReadAndCreate();
        PrintNumberOfWinesOfAllGrapesByFOR();
        PrintNumberOfWinesOfAllGrapesByStreamAPI();
    }


    public static void PrintNumberOfWinesOfAllGrapesByFOR(){
        for(int i = 0; i < grapeList.size(); i++) {
            System.out.println(grapeList.get(i).getName() + " " + grapeList.get(i).getListOfWines().size());

        }
    }

    public static void PrintNumberOfWinesOfAllGrapesByStreamAPI(){
        List<Grape> list = grapeList.stream().peek(z-> System.out.println(z.getName()+" "+ z.getListOfWines().size())).collect(Collectors.toList());

    }


}
