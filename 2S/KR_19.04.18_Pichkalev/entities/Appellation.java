package entities;

import readingCSV.CSVReadable;
import readingCSV.ReaderSaverOfLists;

import java.util.ArrayList;
import java.util.List;

import static readingCSV.ReaderSaverOfLists.wineList;

public class Appellation implements CSVReadable {
    String id;
    String name;
    List listOfWines;
    public Appellation(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void readListOfWine() {
        listOfWines = new ArrayList();
        for (int i = 0; i < wineList.size(); i++) {
            if (wineList.get(i).getAppelation().getName().equals(name))
                listOfWines.add(wineList.get(i));
        }

    }

    public List getListOfWines() {
        return listOfWines;
    }

    public void setListOfWines(List listOfWines) {
        listOfWines = listOfWines;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }
}
