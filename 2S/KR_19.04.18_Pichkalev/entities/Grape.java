package entities;

import readingCSV.CSVReadable;

import java.util.ArrayList;
import java.util.List;

import static readingCSV.ReaderSaverOfLists.wineList;

public class Grape implements CSVReadable {
    String id;
    String name;
    List listOfWines;

    public List getListOfWines() {
        return listOfWines;
    }

    public void setListOfWines(List listOfWines) {
        this.listOfWines = listOfWines;
    }

    public void readListOfWine() {
        listOfWines = new ArrayList();
        for (int i = 0; i < wineList.size(); i++) {
            if (wineList.get(i).getGrape().getName().equals(name))
                listOfWines.add(wineList.get(i));
        }

    }

    public Grape(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
