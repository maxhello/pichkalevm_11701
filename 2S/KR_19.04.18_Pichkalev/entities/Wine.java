package entities;

import readingCSV.CSVReadable;

public class Wine implements CSVReadable {
 Grape grape;
 Appellation appelation;
 String name;

    public Wine(Grape grape, Appellation appelation, String name) {
        this.grape = grape;
        this.appelation = appelation;
        this.name = name;
    }

    public Grape getGrape() {

        return grape;
    }

    public void setGrape(Grape grape) {
        this.grape = grape;
    }

    public Appellation getAppelation() {
        return appelation;
    }

    public void setAppelation(Appellation appelation) {
        this.appelation = appelation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
