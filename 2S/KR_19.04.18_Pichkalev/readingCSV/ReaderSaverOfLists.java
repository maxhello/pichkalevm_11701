package readingCSV;

import entities.Appellation;
import entities.Grape;
import entities.Wine;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReaderSaverOfLists {
    public static List<Wine> wineList;
    public static List<Grape> grapeList;
    public static List<Appellation> appellationList;


    public void startReadAndCreate(){

        grapeList = readCSV("res/grapes.csv", "grapes");
        appellationList = readCSV("res/appellations.csv", "appellations");
        wineList = readCSV("res/wine.csv", "wines");

        for(int i = 0; i < appellationList.size(); i++)
            appellationList.get(i).readListOfWine();

        for (int i = 0; i  < grapeList.size(); i++){
            grapeList.get(i).readListOfWine();
        }


    }
    private List readCSV(String fileName, String whatToCreate) {
        String [] attrs;
        List ListToReturn = new ArrayList();

        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileName)
                    )
            );

            String line = br.readLine();
            int k = 0;
            while (line != null) {
                if(k != 0){

                    attrs = line.split(",");
                    destroyerOfQuotes(attrs);
                    ListToReturn.add(creator(attrs, whatToCreate));
                    line = br.readLine();
                }else{
                    k++;
                    line = br.readLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ListToReturn;
    }

   private CSVReadable creator(String[] attrs, String whatToCreate){

        if(whatToCreate.equals("grapes")){

            return new Grape(attrs[0], attrs[1]);
        }else if(whatToCreate.equals("appellations")){

//            System.out.println(attrs[0] +" " +  attrs[1]);
            return new Appellation(attrs[0], attrs[1]);

        }else if(whatToCreate.equals("wines")){

            Grape grape = null;
            for(int i = 0; i<grapeList.size(); i++){
                if(grapeList.get(i).getName().equals(attrs[1])){
                    grape = grapeList.get(i);

                }
            }

            Appellation appellation = null;

            for(int i = 0; i<appellationList.size(); i++){
//                System.out.println(appellationList.get(i).getId());
                if(appellationList.get(i).getName().equals(attrs[3])){
                    appellation = appellationList.get(i);
                }
            }

                return new Wine(grape, appellation, attrs[5]);
            }

            return null;
        }


    private static void destroyerOfQuotes(String [] arr){
        for(int i = 0; i < arr.length; i++){
            if(arr[i].startsWith("'"))arr[i]=arr[i].substring(1, arr[i].length()-1);
        }
    }
}
