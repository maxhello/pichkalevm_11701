import java.util.Arrays;

public class ExpandTheArraywithStack {
public static void main(String args[]) {
	 LinkedCollection lc = new LinkedCollection();
	int [] arr = {1,2,3,4,5};
	for(int i = 0; i <arr.length; i++) {
		lc.push(arr[i]);
	}
	System.out.println(Arrays.toString(arr));
	for(int i = 0; i <arr.length; i++) {
		arr[i] = (int) lc.pop().getData();
	}
	System.out.println(Arrays.toString(arr));
}
}
