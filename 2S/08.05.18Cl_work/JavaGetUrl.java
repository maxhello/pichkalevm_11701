import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaGetUrl {
	static Logger log = Logger.getLogger(Logger.class.getName());
	static List<String> listOfURL = new ArrayList();
//	https://proekt-montag.su/about/normativ-docs
//	https://docs.spring.io/autorepo/docs/spring-framework/5.0.0.M1/spring-framework-reference/pdf/
	static final String URL_string = "http://kpolyakov.spb.ru/school/ege.htm";
	static final String URL_string2 = "http://kpolyakov.spb.ru";
	// static final String URL_string2 = "https://aldebaran.ru/";

	public static void main(String[] args) throws IOException {

		URL u;
		InputStream is = null;
		DataInputStream dis;
		String s = null;

		try {

			u = new URL(URL_string);

			is = u.openStream();

			dis = new DataInputStream(new BufferedInputStream(is));

			while ((s = dis.readLine()) != null) {
				checkWithRegExp(s);

			}

		} catch (MalformedURLException mue) {
			log.info("error");
			mue.printStackTrace();
			System.exit(1);

		} catch (IOException ioe) {
			log.info("error");
			ioe.printStackTrace();
			System.exit(1);

		} finally {

			try {
				is.close();
			} catch (IOException ioe) {
			}
		}

		
		for (String x : listOfURL) {
			new Thread(new DownloadBooks(x)).start();
			// System.out.println(x);
			System.out.println(x);
			// db.downloadUsingStream(x);
		}
		// getServerURL(URL_string);
	}

	// .re*[\\.pdf]\\""
	// "<a href=\".re*\\.pdf\""
	public static void checkWithRegExp(String userNameString) {
		// Pattern p = Pattern.compile("<a href=\".re*\\.pdf\"");
		Pattern p = Pattern.compile("(<a href=\".*\\.pdf)");
		Matcher m = p.matcher(userNameString);
		while (m.find()) {
			// currentWord = m.group(1);
			System.out.println(m.group(1));

			listOfURL.add(URL_string2 + m.group(1).substring(9));
		}

		// if(m.matches == true)System.out.println();

	}

	// public static void getServerURL(String string) {
	// Pattern p = Pattern.compile(".*/");
	// Matcher m = p.matcher(string);
	// while (m.find()) {
	// // currentWord = m.group(1);
	// System.out.println(m.group(1));
	//
	// }
	//
	// }
}