import java.util.Collection;
import java.util.Iterator;

public class LinkedCollection implements Collection<String>  {

	int size;
	public Node getHead() {
		return head;
	}

	private Node head;
	public boolean add(String data){
		if (head == null) {
			head = new Node(data);
			size++;
			return true;
		}
		Node current = head;
		while (current.next != null){
			current = current.next;  
		}
		current.next = new Node(data);
		size++;
		return true;
	}

	public void prepend(String data){
		Node newHead = new Node(data);
		newHead.next = head;
		head = newHead;
	}

	public boolean remove(Object data) {
		if(head == null) return false;
		if(head.data == (Object) data) {
			head = head.next;
			size--;
			return true;
		}

		Node current = head;
		while (current.next != null) {
			if(current.next.data == data) {
				current.next = current.next.next;
				return true;
			}
			current = current.next;
		}
		return false;
	}
	public void printLinkedList(){

		Node current = head;
		if(current == null) return;
		else System.out.println(current.getData());

		while(current.next != null){
			current = current.next;
			System.out.println(current.getData());
		}
	}
	public void addAll(LinkedCollection another){
		Node anotherHead = another.getHead();
		Node current = head;
		if(current == null) return;
		while(current.next != null){
			current = current.next;

		}
		current.next=anotherHead;
	}

	//	@Override
	//	public void add() {
		//		// TODO Auto-generated method stub
		//		
		//	}

	public boolean contain(String data) {
		if(head == null) return false;
		if(head.data == data) {
			return true;
		}

		Node current = head;
		while (current.next != null) {
			if(current.next.data == data) {
				return true;
			}
		}
		return false;
	}

	//	@Override
	//	public void remove() {
	//		// TODO Auto-generated method stub
	//		
	//	}

	//	public int size(){
	//    int count = 0;
	//        Node current = head;
	//        if(current == null) return -1;
	//        count++;
	//        while(current.next != null){
	//            current = current.next;
	//            count++;
	//            
	//        }
	//        return count;
	//    }

	public int size() {
		return size;
	}

	public void clear() {
		head = null;
	}


	public boolean isEmpty() {
		if( getHead() == null) return false;
		else return true;
	}

	public boolean containsAll(Object array) {
		return false;
	}
	//	public boolean containsAll(Object array) {
	//		if(array instanceof LinkedCollection) {
	//			LinkedCollection  linkedc = (LinkedCollection) array;
	//			
	//		}
	//		// TODO Auto-generated method stub
	//		int count;
	//	       if(head == null) return false;
	//	        if(head.data == data) {
	//	            head = head.next;
	//	            size--;
	//	            return true;
	//	        }
	//
	//	        Node current = head;
	//	        while (current.next != null) {
	//	            if(current.next.data == data) {
	//	                current.next = current.next.next;
	//	                return true;
	//	            }
	//	            current = current.next;
	//	        }
	//		return false;
	//	}
	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<String> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends String> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}


}




class Node {
	Node next;

	public String getData() {
		return data;
	}

	String data;
	public Node(String data){
		this.data=data;
	}


}