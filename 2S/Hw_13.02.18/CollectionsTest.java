import org.junit.Assert;
import org.junit.Test;
public class CollectionsTest {
	@Test
	public void newCollectionShouldHaveSize0() {
		LinkedCollection iac = new LinkedCollection();
		Assert.assertEquals(0, iac.size());

	}
	
	@Test
	public void newCollectionShouldHaveNode0() {
		LinkedCollection iac = new LinkedCollection();
		Assert.assertEquals(null, iac.getHead());
		

	}
	
	@Test
	public void newCollectionShouldAddSommObject() {
		LinkedCollection iac2 = new LinkedCollection();
		iac2.add("T1");
		iac2.add("T2");
		
		Assert.assertEquals(2, iac2.size());
	}
	
	//ToDo
	@Test
	public void newCollectionShouldAddAndResizeArray() {
		IntArrayCollection iac2 = new IntArrayCollection();
		iac2.add(5);
		iac2.add(6);
		for(int i = 0;i<102; i++) {
			iac2.add(i);
		}
	}
	
	
}
