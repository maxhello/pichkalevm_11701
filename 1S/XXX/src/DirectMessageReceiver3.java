
import javax.jms.*;

import com.sun.messaging.ConnectionFactory;
import com.sun.messaging.ConnectionConfiguration;


public class DirectMessageReceiver3 implements MessageListener{

	ConnectionFactory factory = new com.sun.messaging.ConnectionFactory();  
	JMSConsumer consumer;
	
	DirectMessageReceiver3(){
		try( JMSContext context = factory.createContext("admin","admin")){
			factory.setProperty(ConnectionConfiguration.imqAddressList, "mq://127.0.0.1:7676,mq://127.0.0.1:7676");
		    
			Destination mars =
					context.createTopic("Mars");
					consumer = context.createConsumer(mars);
		    
			consumer.setMessageListener(this);
		      
		      System.out.println("RECEIVER3: Listening to the server...");
		      
		      // Keep the program running - wait for messages
		      Thread.sleep(100000);
		    
		   } catch (InterruptedException e){
	           System.out.println("Error: " + e.getMessage());
	       }
		    catch (JMSException e){
		           System.out.println("Error: " + e.getMessage());
		    } 
	}

    public void onMessage(Message msg){
    	
      try{
       System.out.println("Got the text message from the Mars: " +
                          msg.getBody(String.class));
       
      
       
      } catch (JMSException e){
    	  System.err.println("JMSException: " + e.toString());
      }
    }

 public static void main(String[] args){
	  new DirectMessageReceiver3();
 }	 
}