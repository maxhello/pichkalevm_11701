
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 3 Task 12
 */
public class Main {
    public static void main(String[] args) throws NotNaturalNumberException {
        Number[] num = new Number[3];
        num[0] = new VeryLongNumber(8);
        num[1] = new SimpleLongNumber(3);
        num[2] = new SimpleLongNumber(1);
        Number nums = num[0];
        
        System.out.println(nums.toString());
        System.out.println(num[2].compareTo(num[1]));
        for (int i = 1; i< num.length; i++){
        	nums.add(num[i]);
        }
    }
}