
/**
 * @author Pichkalev Max
 * 11-701
 * for Problem Set 3 Task 12
 */
public class NotNaturalNumberException extends Exception {
    private int num;
    public NotNaturalNumberException(String message) {
        super(message);
    }
    public int getNum() {
        return num;
    }


}