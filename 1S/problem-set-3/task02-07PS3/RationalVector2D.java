
public class RationalVector2D {
	/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	static RationalFraction r1;
	static RationalFraction r2;


	RationalVector2D() {

	}

	RationalVector2D(RationalFraction r1, RationalFraction r2){	
		this.r1 = r1;
		this.r2 = r2;
	}

	static RationalVector2D add(RationalVector2D rv1, RationalVector2D rv2) {
		RationalVector2D rv = new RationalVector2D();
		rv.r1.setX(rv1.r1.getX() + rv2.r1.getX());
		rv.r1.setY(rv1.r1.getY() + rv2.r1.getY());
		rv.r2.setX(rv1.r2.getX() + rv2.r2.getX());
		rv.r2.setY(rv1.r2.getY() + rv2.r2.getY());
		return rv;

	}

	public String toString() {
		return r1.toString() +" and "+ r2.toString(); 
	}


	public static double lengthVec() {
		double length = r1.getX()/r1.getY()*r1.getX()/r1.getY()+r2.getX()/r2.getY()*r2.getX()/r2.getY();
		return Math.sqrt(length);

	}

	public static RationalFraction scalarProduct(RationalVector2D rv1, RationalVector2D rv2) {

		RationalFraction rf = new RationalFraction();

		rf.setX(rv1.r1.getX()*rv2.r1.getX());
		rf.setY(rv1.r2.getY()*rv2.r2.getY());
		return rf;
	}
	public static void main(String args[]) {
		RationalFraction r1 = new RationalFraction(2,3);
		RationalFraction r2 = new RationalFraction(2,3);
		RationalVector2D rv1 = new RationalVector2D(r1,r2);
		RationalVector2D rv2 = new RationalVector2D(r1,r2);
		add(rv1,rv2);
		lengthVec();
		scalarProduct(rv1,rv2);
	}
}
