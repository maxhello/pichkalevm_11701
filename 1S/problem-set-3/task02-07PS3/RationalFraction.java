
 class RationalFraction {
	 /**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	private static int x;
	private static int y;
	RationalFraction(){

	}

	RationalFraction(int x, int y){
		if(y == 0) throw new ArithmeticException("division by zero");
		this.x = x;
		this.y = y;
	}



	//	void reduce(int x, int y){
	//		int bufX=x, bufY=y;
	//		for(int i = x; i>1; i--) {
	//			while(true)
	//			if(x%i == 0) 
	//				if (y%i == 0) {
	//					x=x/i;
	//					y=y/i;
	//				}
	//		}
	//	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	private static void reduce(){
		int divisor;
		divisor = gcd(x, y);
		x = x / divisor;
		y = y / divisor;
	}

	static int gcd(int x, int y){
		int r;
		while (y != 0) {
			r = x % y;
			x = y;
			y = r;
		}
		return x;
	}

	public static void times(RationalFraction rat){
		x = x * rat.x;
		y = y * rat.y;
		reduce();
	}

	public static void divBy(RationalFraction rat){
		x = x / rat.x;
		y = y / rat.y;
		reduce();
	}

	public static void minus(RationalFraction rat){
		int greatdenom = rat.y * y;       
		int multx = greatdenom / rat.y;
		int mult = greatdenom / y;
		y = rat.y * y;
		if (rat.x > x){
			x = (rat.x * multx) - (x * mult);
		}
		else {
			x = (x * mult) - (rat.x * multx);
		}
		reduce();
	}


	public String toString(){
		if (y == 1){
			return x + "";
		}
		else{
			return x + " / " + y;
		}
	}

	public static void plus(RationalFraction rat){
		int greatdenom = rat.y * y;       
		int multx = greatdenom / rat.y;
		int mult = greatdenom / y;
		y = rat.y * y;
		x = (rat.x * multx) + (x * mult);
		reduce();
	}

	public static double value() {
		double dec = x/y;
		return dec;
	}


	public static void main(String args[]) { 
		RationalFraction rf = new RationalFraction(2,3);
		times(rf);
		divBy(rf);
		minus(rf);
		plus(rf);
		value();
	}

}
