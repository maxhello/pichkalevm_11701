
public class RationalMatrix2x2 {
	/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	static RationalFraction[][] rfMat = new RationalFraction[2][2];
	
	RationalMatrix2x2() {
		
	}
	
	RationalMatrix2x2(RationalFraction rf){
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++) {
				rfMat[i][j] = new RationalFraction(rf.getX(),rf.getY());
			}
	}
	
	
	
	static RationalMatrix2x2 add(RationalMatrix2x2 rm, RationalMatrix2x2 rm2) {
		RationalMatrix2x2 rmN = new RationalMatrix2x2();
		for(int i = 0; i < 2; i++)
			for(int j = 0; j < 2; j++) {
				rmN.rfMat[i][j].setX(rm.rfMat[i][j].getX()+rm2.rfMat[i][j].getX()); 
				rmN.rfMat[i][j].setY(rm.rfMat[i][j].getY()+rm2.rfMat[i][j].getX()); 
				
			}
		return rmN;
	}
	
	
	static RationalMatrix2x2 mult(RationalMatrix2x2 a, RationalMatrix2x2 b) {
		int m = a.rfMat.length;
		RationalMatrix2x2 c = new RationalMatrix2x2();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				for (int k = 0; k < m; k++) {	
					
					c.rfMat[i][j].setX(a.rfMat[i][k].getX() * b.rfMat[k][j].getX());
					c.rfMat[i][j].setY(a.rfMat[i][k].getY() * b.rfMat[k][j].getY());
					
				}
		
				return c;
	}
	
	
	static RationalVector2D multVector(RationalVector2D a) {
		RationalFraction rf1 = new RationalFraction();
		RationalFraction rf2 = new RationalFraction();
		
		rf1.setX(rfMat[0][0].getX()*a.r1.getX()/rfMat[0][1].getX()*a.r1.getX());
		rf1.setY(rfMat[0][0].getY()*a.r1.getY()/rfMat[0][1].getY()*a.r1.getY());
		
		rf2.setX(rfMat[1][0].getX()*a.r2.getX()/rfMat[1][1].getX()*a.r2.getX());
		rf2.setY(rfMat[1][0].getY()*a.r2.getY()/rfMat[1][1].getY()*a.r2.getY());
		
 return new RationalVector2D(rf1, rf2);
		
       
		
	}
	
	public static void main(String args[]) {
		RationalMatrix2x2 rm = new RationalMatrix2x2();
		RationalMatrix2x2 rm2 = new RationalMatrix2x2();
		RationalVector2D a = new RationalVector2D();
		add(rm,rm2);
		mult(rm,rm2);
		multVector(a);
	}
	
}
