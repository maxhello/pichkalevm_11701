class Matrix2x2 {
	/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	static double mat[][] = new double[2][2];
	Matrix2x2(){
	}

	Matrix2x2(double num){
		for(int i = 0; i<mat.length; i++)
			for(int j = 0; j<mat[i].length; j++){
				mat[i][j] = num;
			}
	}
	Matrix2x2(double mat[][]){
		this.mat=mat;
	}

	public static Matrix2x2 add(Matrix2x2 a, Matrix2x2 b) {
		int m = a.mat.length;
		Matrix2x2 c = new Matrix2x2();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				c.mat[i][j] = a.mat[i][j] + b.mat[i][j];
		return c;
	}

	public static Matrix2x2 sub(Matrix2x2 a, Matrix2x2 b) {
		int m = a.mat.length;
		Matrix2x2 c = new Matrix2x2();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				c.mat[i][j] = a.mat[i][j] - b.mat[i][j];
		return c;
	}

	public static Matrix2x2 mult(Matrix2x2 a, Matrix2x2 b) {
		int m = a.mat.length;
		Matrix2x2 c = new Matrix2x2();
		
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				for (int k = 0; k < m; k++)
					c.mat[i][j] += a.mat[i][k] * b.mat[k][j];
		return c;
	}

	public static Matrix2x2 multiply(Matrix2x2 a, double g) {
		int m = a.mat.length;
		Matrix2x2 c = new Matrix2x2();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				c.mat[i][j] += a.mat[i][j] * g;
		return c;
	}

	public static double[][] transpose(double[][] a) {
		int m = a.length;
		double[][] b = new double[m][m];
		for (int i = 0; i < m; i++)
			for (int j = 0; j < m; j++)
				b[j][i] = a[i][j];
		return b;
	}
	
	public static Matrix2x2 inversion(Matrix2x2 A, int N) {
		double temp;
		double [][] E = new double [N][N];
		
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++) {
				E[i][j] = 0f;
				if (i == j)
					E[i][j] = 1f;
			}

		for (int k = 0; k < N; k++) {
			temp = A.mat[k][k];
			for (int j = 0; j < N; j++) {
				A.mat[k][j] /= temp;
				E[k][j] /= temp;
			}
			
			for (int i = k + 1; i < N; i++) {
				temp = A.mat[i][k];
				for (int j = 0; j < N; j++) {
					A.mat[i][j] -= A.mat[k][j] * temp;
					E[i][j] -= E[k][j] * temp;
				}
			}
		}

		for (int k = N - 1; k > 0; k--) {
			for (int i = k - 1; i >= 0; i--) {
				temp = A.mat[i][k];
				for (int j = 0; j < N; j++) {
					A.mat[i][j] -= A.mat[k][j] * temp;
					E[i][j] -= E[k][j] * temp;
				}
			}
		}

		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				A.mat[i][j] = E[i][j];
		return A;
	}
	
	public static double determinant(double a[][], int n){
		double det = 0; int sign = 1, p = 0, q = 0;

		if(n==1){
			det = a[0][0];
		}
		else{
			double b[][] = new double[n-1][n-1];
			for(int x = 0 ; x < n ; x++){
				p=0; q=0;
				for(int i = 1;i < n; i++){
					for(int j = 0; j < n;j++){
						if(j != x){
							b[p][q++] = a[i][j];
							if(q % (n-1) == 0){
								p++;
								q=0;
							}
						}
					}
				}
				det = det + a[0][x] *
	                              determinant(b, n-1) *
	                              sign;
				sign = -sign;
			}
		}
		return det;
	}
	
	
	static Vector2D multVector(Vector2D vec) {
		double x = 0,y = 0;
		
				x = x + mat[0][0]*vec.getX() + mat[0][1]*vec.getY();		

				y = y + mat[1][0]*vec.getX() + mat[1][1]*vec.getY();		
			
			Vector2D vector2D = new Vector2D(x,y);
			return vector2D;

	}
	public static void main(String args[]) { 
		Matrix2x2 a = new Matrix2x2();
		Matrix2x2 b = new Matrix2x2();
		Vector2D vec = new Vector2D(2,3);
		add(a,b);
		sub(a,b);
		mult(a,b);
		multiply(a,3);
		transpose(mat);
		inversion(a,2);
		determinant(mat, 2);
		multVector(vec);
	}
	
}


