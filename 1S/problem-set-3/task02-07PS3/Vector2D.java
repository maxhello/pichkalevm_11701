
 public class Vector2D {
	 /**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	 
private static double x;
private static double y;

Vector2D(){
	
}

Vector2D(double x, double y){
	
	this.x = x;
	this.y = y;
	
}
public double getX() {
	return x;
}

public void setX(double x) {
	this.x = x;
}

public double getY() {
	return y;
}

public void setY(double y) {
	this.y = y;
}

public static Vector2D sum(Vector2D vec1, Vector2D vec2) {
	double newX = vec1.x+vec2.x;
	double newY = vec1.y + vec2.y;
	Vector2D vecNew = new Vector2D(newX, newY);
	return vecNew;
	
}

public static Vector2D sub(Vector2D vec1, Vector2D vec2) {
	double newX = Math.abs(vec1.x - vec2.x);
	double newY = Math.abs(vec1.y - vec2.y);
	Vector2D vecNew = new Vector2D(newX, newY);
	return vecNew;
	
}

public static Vector2D mult(Vector2D vec1, double k) {
	double newX = vec1.x*k;
	double newY = vec1.y*k;
	Vector2D vecNew = new Vector2D(newX, newY);
	return vecNew;
	
}

public String toString() {
	return "x coordinate is: "+ x +"y coordinate is: "+y; 	
}

public static double lengthVec() {
	double length = x*x+y*y;
	return Math.sqrt(length);
	
}

public static double scalarProduct(Vector2D vec1, Vector2D vec2) {
	 double pro = vec1.x*vec2.x + vec1.y+vec2.y;
	return pro;
}


public static void main(String args[]) {
	Vector2D v1 = new Vector2D(1,2);
	Vector2D v2 = new Vector2D(1,3);
	
	sum(v1,v2);
	sub(v1,v2);
	mult(v1,2);
	lengthVec();
	scalarProduct(v1,v2);
	
}
}
