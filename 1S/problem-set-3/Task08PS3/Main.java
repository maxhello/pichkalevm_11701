
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class Main {
    private static ArrayList<String> sites = new ArrayList<>();
    private static ArrayList<Double> chrome = new ArrayList<>();
    private static ArrayList<Double> internetExplorer = new ArrayList<>();
    private static ArrayList<Double> firefox = new ArrayList<>();
    private static ArrayList<Double> safari = new ArrayList<>();
    static ArrayList<WebSites> webSites = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        readFile();
        createWebSites();
        System.out.println("the fastest site is: "+ WebSites.schnelleSite().getName());
        System.out.println("the fastest web is: "+WebSites.fastestBrowser());
        System.out.println(WebSites.totalTime());
    }



    private static void createWebSites() {
        for (int i = 0; i < sites.size(); i++) {
            webSites.add(new WebSites(sites.get(i), chrome.get(i), internetExplorer.get(i), firefox.get(i), safari.get(i)));
        }
    }

    private static void readFile() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Browser_Speed.csv"));
        String text = sc.nextLine();
        text = text.replaceAll("\\.", "");
        String[] textArr = text.split(",");

        for (int i = 0; i < textArr.length - 3; i++) {
            sites.add(textArr[i + 3]);
        }

        sc.nextLine();
        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            chrome.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            internetExplorer.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            firefox.add(Double.valueOf(textArr[i + 3]));
        }

        text = sc.nextLine();
        textArr = text.split(",");
        for (int i = 0; i < textArr.length - 3; i++) {
            safari.add(Double.valueOf(textArr[i + 3]));
        }

    }

}
