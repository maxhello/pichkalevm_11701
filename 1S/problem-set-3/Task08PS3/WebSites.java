
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class WebSites {
        private String name;

        private double chrome;
        private double internetExplorer;
        private double firefox;
        private double safari;

        public String getName() {
            return name;
        }

        public double getChrome() {
            return chrome;
        }

        public double getInternetExplorer() {
            return internetExplorer;
        }

        public double getFirefox() {
            return firefox;
        }

        public double getSafari() {
            return safari;
        }


        public WebSites(String name, double chrome, double internetExplorer, double firefox, double safari) {
            this.name = name;
            this.chrome = chrome;
            this.internetExplorer = internetExplorer;
            this.firefox = firefox;
            this.safari = safari;
        }


        public static WebSites schnelleSite() {
            WebSites fastestSite = null;
            double min = Double.MAX_VALUE;
            for (WebSites site : Main.webSites) {
                double sum = 0;
                for (int i = 0; i < 4; i++) {
                    sum = site.getChrome() + site.getFirefox() + site.getInternetExplorer() + site.getSafari();
                    sum /= 4;
                }
                if (sum < min) {
                    min = sum;
                    fastestSite = site;
                }
            }
            return fastestSite;
        }
        
        public static double totalTime() {
            double sum = 0;
            for (WebSites site : Main.webSites) {
                sum = sum + site.getChrome() + site.getFirefox() + site.getInternetExplorer() + site.getSafari();
            }
            return sum = sum / 36;
        }

        public static String fastestBrowser() {
            double chrome = 0;
            double explorer = 0;
            double firefox = 0;
            double safari = 0;
            for (WebSites site : Main.webSites) {
                chrome += site.getChrome();
                explorer += site.getInternetExplorer();
                firefox += site.getFirefox();
                safari += site.getSafari();
            }
            if (chrome < explorer && chrome < firefox && chrome < safari) {
                return "Chrome";
            }
            if (explorer < chrome && explorer < firefox && explorer < safari) {
                return "Explorer";
            }
            if (firefox < explorer && firefox < chrome && firefox < safari) {
                return "Firefox";
            }
            else  {
                return "Safari";
            }
        }

      
    }