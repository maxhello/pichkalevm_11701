import java.util.Date;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class Subscriptions {
	private User subscription;
	private User subscriptor;
	
    public void setSubsctiptor(User subscriptor) {
        this.subscriptor = subscriptor;
    }
    public User getSubscriptor() {
        return subscriptor;
    }

    public User getSubscription() {
        return subscription;
    }
    
    public void setSubscription(User subscription) {
        this.subscription = subscription;
        
    }
}