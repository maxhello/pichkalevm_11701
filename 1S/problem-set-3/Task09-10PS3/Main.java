

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */

class MessagesDateComparator implements Comparator<Message> {

    @Override
    public int compare(Message o1, Message o2) {
        return -1 * o1.getDate().compareTo(o2.getDate());
    }
}

public class Main {
    public static void printChat(String username1, String username2) {
        ArrayList<Message> msgs = new ArrayList<>();
        for (Message message : Database.messages) {
            if (message.getReceiver().getUsername().equals(username1) &&
                message.getSender().getUsername().equals(username2) ||
                message.getReceiver().getUsername().equals(username2) &&
                message.getSender().getUsername().equals(username1)) {
                //                System.out.println(message);
                msgs.add(message);
            }
        }
        Collections.sort(msgs, new MessagesDateComparator());
        System.out.println(msgs);
    }
    
    public static void printFriends() {
    	for (int i=0; i < Database.subs.length; i++) {
    		long subtionId = Database.subs[i].getSubscription().getId();
    		long subtorId = Database.subs[i].getSubscriptor().getId();

    		for(int j=0; j< Database.subs.length; j++) {
    			if(subtionId == Database.subs[j].getSubscriptor().getId() && subtorId == Database.subs[j].getSubscription().getId())
    				System.out.println("first: "+ Database.subs[i].getSubscription().getUsername()+" his friend " + Database.subs[i].getSubscriptor().getUsername());
    			
    		}
    	}
    }
    
    public static void genderMaxMessage() {
    	int f = 0;
    	int m = 0;
    	 for (Message message : Database.messages) {
    		 if(message.getSender().getGender() == User.Gender.MALE) { 
    			 f++; 
    		 } else m++;
    		 
    }
    	 if(f>m)System.out.println("woman won"); else
    		 if(f==m) System.out.println("equal"); else
    			 if(f<m) System.out.println("man won");
    }
    
    public static void readMessage() {
    	int byManReadByWoman = 0;
    	int allByMan = 0;
    	int byWomanReadByMan = 0;
    	int allByWoman = 0;
    	
    	int mansMesPercent = 0;
    	int womansMesPercent = 0;
    	
    	
    	
    	for (Message message : Database.messages) {
   		 if(message.getSender().getGender() == User.Gender.MALE & message.getReceiver().getGender() == User.Gender.FEMALE) {
   			 
   			 if(message.getStatus() == Message.Status.READ) {
   				 byManReadByWoman++;
   				 
   			 }
   			allByMan++;
   		 }
   		 
   		if(message.getSender().getGender() == User.Gender.FEMALE & message.getReceiver().getGender() == User.Gender.MALE) {
   		 if(message.getStatus() == Message.Status.READ) {
   			byWomanReadByMan++;
				 
			 }
   		allByWoman++;
   		}
   }
    	mansMesPercent = byManReadByWoman/allByMan*100;
    	womansMesPercent = byWomanReadByMan/allByWoman*100;
    	System.out.println("man's Messages Read By woman: " + mansMesPercent +"; woman's Messages Read By man: " + womansMesPercent);
    	
    }
    

    public static void main(String[] args)
            throws FileNotFoundException, ParseException {

        Database.loadUsers();
        Database.loadMessages();
        Database.loadSubs();
        //System.out.println(Arrays.toString(Database.users));
        //System.out.println(Arrays.toString(Database.messages));
        //System.out.println(Database.messages[1].getSender().getEmail());
        printChat("daler", "dima");
        printFriends();
        genderMaxMessage();
        readMessage();
    }
}