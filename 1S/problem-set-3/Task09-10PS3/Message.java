
import java.util.Date;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class Message {
    private User sender;
    private User receiver;
    private Date date;
    private String text;
    private Status status;
    enum Status{READ, UNREAD}

    @Override
    public String toString() {
        return "Message{" +
                "sender=" + sender.getUsername() +
                ", receiver=" + receiver.getUsername() +
                ", text='" + text + '\'' +
                ", status='" + status + '\'' +
                ", date=" + date +
                '}';
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }
    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public Date getDate() {
        return date;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Status getStatus() {
        return status;
    }
}