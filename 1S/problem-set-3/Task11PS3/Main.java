/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class Main {
public static void main(String args[]) {
	
	Ships[] sh = new Ships[2];
	sh[0] = new HC();
	sh[1] = new Pirate();
	
	sh[0].shouting();
	sh[1].shouting();
	
}
}
