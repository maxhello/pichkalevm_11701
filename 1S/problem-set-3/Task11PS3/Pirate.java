/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
class Pirate extends Ships{       //heavy cruiser
	int structure=35;
	double chance = chance(0.6, 0.4);
	int damage =  (int) (damages(40,30)* chance) ;
	
	Pirate(){}
	Pirate(int damageOUT){

		structure = destruction(structure, damageOUT);
	}
	
	 public void shouting() {
		 System.out.println("I am the Pirate!");
	 }

}