
public class Gamer {
	/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
	private String name;
	private int hp;
	
	Gamer(String name){
		hp = 20;
		this.name = name;
	}
	
	public String getName() {
		return name;	
	}
	public int getHP() {
		return hp;	
	}
	public void setHP(int damage) {
		hp = hp - damage;
		
	}
	
}
