import java.util.Scanner;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */
public class Task09PS2 {
	
    public static void backtracking(int x, String s, int y) {
        if (s.length() == x) {
            System.out.println(s);
        } else if (s.length() == 0) {
            for (char c = 'a'; c <= 'z'; c += 1) {
                backtracking(x, s + c, y);
            }
        } else {
            for (char c = 'a'; c <= 'z'; c += 1) {
                if (Math.abs((int) c - s.charAt(s.length() - 1)) == y) {
                    backtracking(x, s + c, y);
                }
            }
        }
    }
	
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = "";
        int x = sc.nextInt();
        int y = sc.nextInt();
        backtracking(x, s, y);
    }
}