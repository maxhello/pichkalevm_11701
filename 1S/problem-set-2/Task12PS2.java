import java.util.Scanner;

/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 12
 */
public class Task12PS2 {
	public static int k;
	public static int n;

	public static void GeneratePopulation(String[] population, int n) {
		for (int i = 0; i < population.length; i++) {
			population[i] = GenerateString(n);
		}
	}

	public static boolean checkPopulation(String[] population) {
		boolean check = true;
		for (String aPopulation : population) {
			check &= get�oefficient(aPopulation) > 98.0;
		}
		return check;
	}

	public static double get�oefficient(String population) {
		int numTrue = 0;
		int a;
		int b;
		for (int i = 0; i < population.length() - 1; i++) {
			a = (int) population.charAt(i);
			b = (int) population.charAt(i + 1);
			if (Math.abs(a - b) == k) {
				numTrue += (100/(n-1));
			}
		}

		return (double) numTrue;
	}

	public static String GenerateString(int n) {
		String str = "";
		for (int i = 0; i < n; i++) {
			str += (char) ('a' + Math.random() * 25);
		}
		return str;
	}

	public static void ShowPopulation(String[] population) {
		for (int i = 0; i < population.length; i++) {
			System.out.print(population[i] + get�oefficient(population[i]) + "     ");
		}
		System.out.println("");
	}

	public static void makeChildren(String[] population, String[] childPopulation, int n) {
		int parent;
		for (int i = 0; i < childPopulation.length; i++) {
			childPopulation[i] = "";
			for (int ch = 0; ch < n; ch++) {
				parent = (int) (Math.random() * 2);
				childPopulation[i] += population[2 * i + parent].charAt(ch);
			}
		}
	}

	public static void sorting(String[] population) {
		String forChange = "";
		double fChange;
		double[] koef = new double[population.length];
		for (int i = 0; i < population.length; i++) {
			koef[i] = get�oefficient(population[i]);
		}
		for (int i = population.length - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (koef[j] < koef[j + 1]) {
					forChange = population[j];
					population[j] = population[j + 1];
					population[j + 1] = forChange;
					fChange = koef[j];
					koef[j] = koef[j + 1];
					koef[j + 1] = fChange;
				}
			}
		}

	}

	public static void changing(String[] population, String[] childPopulation) {
		String Change;
		for (int i = 0; i < childPopulation.length; i++) {
			for (int j = 0; j < population.length && i < childPopulation.length; j++) {
				if (get�oefficient(childPopulation[i]) > get�oefficient(population[j])) {
					Change = population[j];
					population[j] = childPopulation[i];
					childPopulation[i] = Change;
					i++;
				}
			}
		}
	}



	public static void Selection(String[] population, String[] childrenPopulation, int n) {
		do {
			makeChildren(population, childrenPopulation, n);
			sorting(childrenPopulation);
			System.out.println("Population: ");
			;
			ShowPopulation(population);
			System.out.println("Children ");
			ShowPopulation(childrenPopulation);
			changing(population, childrenPopulation);

		} while (!checkPopulation(population));

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		n = sc.nextInt();
		k = sc.nextInt();
		int num = 90;
		String[] population = new String[num];
		String[] childrensPopulation = new String[num / 2];
		GeneratePopulation(population, n);
		sorting(population);
		Selection(population, childrensPopulation, n);
		System.out.println(" Result");
		System.out.println("Population:");
		ShowPopulation(population);


	}
}