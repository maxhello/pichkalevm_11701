import java.util.regex.Matcher;
import java.util.regex.Pattern;
	/**
	 * @author Pichkalev Max
	 * 11-701
	 * Problem Set 2 Task 09
	 */

public class Task02PS2{

	public static void main(String args[]){

		Pattern p = Pattern.compile("[+-]?(0|[1-9][0-9]*)([.,][0-9]*(\\([0-9]*[1-9]+[0-9]*\\)|[1-9]))?");
		Matcher m = p.matcher("-0.5");


		while(m.find()) {
			System.out.println(m.group());

		}
	}
}