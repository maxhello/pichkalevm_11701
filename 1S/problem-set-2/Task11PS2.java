import java.util.*;
import java.lang.*;
import java.io.*;


public class Task11PS2 {
	/**
	 * @author Pichkalev Max
	 * 11-701
	 * Problem Set 2 Task 09
	 */
	public static void main (String[] args) {
		String[] ar = {"betta", "franklen", "abc","acc", "avc"};
		System.out.println(Arrays.toString(ar));
		String a = "acc";
		String b = "abc";
		System.out.println("first is: "+ comparator(a,b));
		String buf;
		for(int i = 0; i < ar.length; i++) {
			for(int j = 0; j < ar.length-1; j++) {
				if(!comparator(ar[j],ar[j+1])) {
					buf = ar[j];
					ar[j] = ar[j+1];
					ar[j+1] = buf;
				}
			}
		}
		System.out.println(Arrays.toString(ar));
	}

	private static boolean comparator(String st1, String st2) {
		int k = 0;
		int a = 0;
		int b = 0;
		int minLength = min(st1.length(),st2.length());
		for(int i = 0; i < minLength; i++) {
			k++;
			if(st1.charAt(i) > st2.charAt(i)) return false; else
				if(st1.charAt(i) < st2.charAt(i)) return true; else
					if(st1.charAt(i) == st2.charAt(i) & k == minLength) 
						if(st1.length() == st2.length()) return true; else
							if(st1.length()<st2.length()) return true; else
								if(st1.length() > st2.length()) return false;

		}
		return false;

	}

	private static int abs(int x) {
		return (x < 0) ? -x : x;
	}

	private static int min(int x, int y) {
		if (x < y) return x; else
			return y;
	}
}
