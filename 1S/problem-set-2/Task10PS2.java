import java.util.Arrays;

public class Task10PS2 {
	
	/**
	 * @author Pichkalev Max
	 * 11-701
	 * Problem Set 2 Task 09
	 */
	
	public static void main(String args[]) {
		String [] ar = {"america","europe", "asia"}; 
		String buffer;
		for(int i = 0; i < ar.length; i++) {
			for(int j = 0; j < ar.length-1; j++) {
				if(comparator(ar[j],ar[j+1]) == ar[j]) {
					buffer = ar[j+1];		   
				} else 
					buffer = ar[j];
				ar[j] = comparator(ar[j], ar[j+1]);
				ar[j+1] = buffer;
			}

		}
		System.out.println(Arrays.toString(ar));
	}

	private static String comparator(String st1, String st2) {
		int k = 0;
		int a = 0;
		int b = 0;
		int minLength = min(st1.length(),st2.length());
		for(int i = 0; i < minLength; i++) {
			k++;
			if(st1.charAt(i) > st2.charAt(i)) return st2; else
				if(st1.charAt(i) < st2.charAt(i)) return st1; else
					if(st1.charAt(i) == st2.charAt(i) & k == minLength) 
						if(st1.length() == st2.length()) return st1; else
							if(st1.length()<st2.length()) return st1; else
								if(st1.length() > st2.length()) return st2;

		}

		return "smth wrong";

	}

	private static int abs(int x) {
		return (x < 0) ? -x : x;
	}

	private static int min(int x, int y) {
		if (x < y) return x; else
			return y;

	}

}