import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */

public class Task03PS2{
	public static void main(String args[]){
		String[][] array = {{"1010"},{"1111"},{"1000"}}; 
		for(int i=0; i<array.length; i++)
			for(int j=0; j<array[i].length;j++) {
				System.out.println(comparator(array[i][j]));
			}


		//    System.out.println(m.matches());



	}

	public static boolean comparator(String str) {
		Pattern p = Pattern.compile("^((01)*)$|(^(10)*)|(^0*$)|(^1*$)|(^(101)*$)|(^(010)*$)");
		Matcher m = p.matcher(str);

		return m.matches();
	}	
}