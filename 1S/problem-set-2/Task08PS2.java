import java.util.*;
import java.lang.*;
import java.io.*;
/**
 * @author Pichkalev Max
 * 11-701
 * Problem Set 2 Task 09
 */

public class Task08PS2 {

	public static long gcd(long x, long y) {
		return (y != 0) ? gcd(y, x%y) : x;
	}
	public static void main (String[] args) {
		int array[] = {1,2,3,7};

		System.out.println(isAllGcd(array));
	}


	public static boolean isAllGcd(int[] array) {
		int k = 0;
		for (int i = 0; i < array.length-1; i++)
		{ 
			if (gcd(array[i+1],array[i]) == 1) {
				
				k++;
			} else return false;
		}

		return true; 
	}
}