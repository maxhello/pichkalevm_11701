import java.util.Scanner;

public class Task08PS1 {
	static double abs(double z) {
		return (z<0)?-z:z;
	}

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		double x = Double.parseDouble(sc.nextLine());

		double denominator = 10; 
		double sum = x; 
		double numerator = x; 
		int k = 1;
		final double e = 1e-9;
		double quotient = 1;
		int fact = 1; 

		while(abs(quotient) > e) {
			fact = fact * (2 * k * (2 * k - 1)) * (-1);
			numerator *= x * x * x * x;

			denominator = fact * (4*k+1);

			quotient = (double)numerator/denominator;
			k++;
			sum = sum + quotient; 

		}

		System.out.println(sum);	
	}
}
