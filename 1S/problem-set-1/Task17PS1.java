import java.util.Arrays;

public class Task17PS1 {
public static void main(String args[]) {
	int [] xxx = {1,2,3,4,5,6,7,8,9};
   	

	turn(xxx);
	System.out.println(Arrays.toString(xxx));
	
}
public static void turn(int[] massive) {
    for (int i = 0; i < massive.length / 2; i++) {
        int tmp = massive[i];
        massive[i] = massive[massive.length - i - 1];
        massive[massive.length - i - 1] = tmp;
    }
}
}
