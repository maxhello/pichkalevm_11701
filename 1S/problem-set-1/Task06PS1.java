import java.util.Scanner;
//---
public class Task06PS1 {
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int numberOfAsterisks = n;
		int numberOfZero = 1;
		int k = 1;
		for(int j = 1; j <=2*n+2; j++ ) {

			for(int i=1; i <= numberOfAsterisks; i++) {
				System.out.print("*");

			}

			for(int i=1; i <= numberOfZero; i++) {
				System.out.print("0");

			}
			
			for(int i=1; i <= numberOfAsterisks; i++) {
				System.out.print("*");

			}
			System.out.println();
		
			if(j > n+1) {
				numberOfAsterisks = numberOfAsterisks + k;
				numberOfZero = numberOfZero - 2*k;
			}

			if(j < n+1) {
				numberOfAsterisks = numberOfAsterisks - k;
				numberOfZero = numberOfZero +2* k;
				
			}
		}	
	}
}
