import java.util.Arrays;
import java.util.Scanner;

public class Task15PS1 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String args[]) {
		System.out.println("capacity of array");
        int n = sc.nextInt();
		int[] array = new int[n];


		for(int i = 0; i<array.length; i++) {
			System.out.println("input the value");
			array[i]= sc.nextInt();

		}
		System.out.println(Arrays.toString(array));
		System.out.println(finder(array));

	}


	static boolean finder(int [] a) {
		int counter = 0;
		int uncountable = 0;
		int countable = 0;
		for(int i = 0; i<a.length; i++)
			while(a[i] > 0) {
				if(a[i] % 2 == 0) {
					countable++;
				} else uncountable++;
				if(countable>0 && uncountable>0) {
					uncountable = 0;
					countable = 0;
					break;
				}

				a[i]=a[i]/10;

				if(a[i] == 0 && (uncountable == 3 || uncountable == 5 ||
						countable == 3 || countable == 5)) {
					uncountable = 0;
					countable = 0;
					counter++;
				}

			}
		return counter == 2;

	} 
}