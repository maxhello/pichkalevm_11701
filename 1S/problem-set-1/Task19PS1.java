import java.util.Arrays;
import java.util.Scanner;

public class Task19PS1 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int[][] xxx = new int[2*n+1][2*n+1];
		for(int i=0; i<xxx.length; i++) 
			for(int j = 0; j < xxx[i].length; j++) {
				xxx[i][j] = (int) (Math.random()*10);

			}

		for(int i = 0; i< xxx.length; i++)
			System.out.println(Arrays.toString(xxx[i]));


		int count = 2*n+1-2;
		int position = 1;
		int buf_position = 1;
		for(int i=0; i<n+1; i++) {

			buf_position=position;
			for(int j=0; j<count; j++) {

				xxx[i][buf_position]=0;
				buf_position++;

			}
			position++;
			count-=2;
		}

		count = 2*n+1-2;
		position = 1;
		buf_position = 1;

		for(int i=2*n; i>=n; i--) {


			buf_position=position;
			for(int j=0; j<count; j++) {

				xxx[i][buf_position]=0;
				buf_position++;

			}
			position++;
			count-=2;
		}

		System.out.println();
		for(int i = 0; i< xxx.length; i++)
			System.out.println(Arrays.toString(xxx[i]));

	}

}
