import java.util.Scanner;

public class Task14PS1 {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		int k = sc.nextInt();
		int n = sc.nextInt();
		int c = 1;
		int sum = 0;
		int remainder = 0;
		int degree = 0;
		while(n>0) {
			remainder = n % 10 * degree(k, degree);
			degree++;
			sum+=remainder;
			n=n/10;


		}
		System.out.println(sum);
	}

	static int degree(int number, int degree) {
		for(int i=1; i<degree; i++) {
			number = number*number;

		}
		if(degree == 0)return 1;
		if(degree == 1)return number;

		return number; 

	}

}

