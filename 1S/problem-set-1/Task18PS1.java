import java.util.Arrays;
import java.util.Scanner;

public class Task18PS1 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("plz input n = capacity of array and later k which < n-1");
		int n = sc.nextInt();
		int k = sc.nextInt();

		int[] array = new int[n];

		for(int i = 0; i<array.length; i++) {
			array[i] = (int)(Math.random()*10);

		}
		System.out.println(Arrays.toString(array));
		
		int buf = 0;
		for(int i = 0; i<=k-1; i++) {
			buf = array[i];
			array[i] = array[i+k];
			array[i+k] = buf;
			
		}
		System.out.println(Arrays.toString(array));
	}
}