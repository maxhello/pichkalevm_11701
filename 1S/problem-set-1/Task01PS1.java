import java.lang.Math;
import java.util.Scanner;
import java.io.*;
import java.util.*;
import java.io.FileNotFoundException;

public class Task01PS1 {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner scan=new Scanner(System.in);
		int n = scan.nextInt();
		int count = 0;
		for (int i = 0; i < n; i++) {
			for (int k = 0; k < 2*n-i; k++) {
				System.out.print(" ");
			}
			for (int j = 0; j < i*2+1; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}

		System.out.println("");

		for (int i = 0; i < n; i++) {
            for (int k = 1; k < 3; k++) {
                for (int j = 1; j <= (n - i - 1) * k; j++) {
                    System.out.print(" ");
                }
                System.out.print(" ");
                for (int j = 1; j <= (i + 1) * 2 - 1; j++) {
                    System.out.print("*");
                }
            }

            System.out.println("");
		}
	}
}



