import java.util.Arrays;

public class Task20PS1 {
	public static void main(String args[]) {

		double [][] m = {{1,1,1},{1,1,3},{1,4,6}}; 
		for(int i = 0; i<m.length; i++)
			System.out.println(Arrays.toString(m[i]));//mt1
		System.out.println(gauss(m));	 
	}

	public static double gauss (double[][] mt1) {
		int rows = 3;
		int cols = 3;
		double result = 1;
		double [][] m2 = new double[rows][cols]; 

		for (int i=0; i<rows; i++){
			for (int j=0; j<cols; j++){

				m2[i][j]=mt1[i][j];
			}
		}

		for (int i=0; i<cols; i++){
			for (int x=0; x<rows; x++){
				for (int y=0; y<cols; y++){
					mt1[x][y]=m2[x][y];
				}
			}
			for (int j=i+1; j<rows; j++){
				for (int k=i; k<cols; k++){
					double tmp = m2[j][k]-(m2[i][k]*mt1[j][i])/m2[i][i];
					m2[j][k]=tmp;
				}                
			}

		}
		System.out.println();
		for(int i = 0; i<m2.length; i++)
			System.out.println(Arrays.toString(m2[i]));
		for (int x=0; x<rows; x++){
			result = result*m2[x][x];
		}
		return result;
	}
}