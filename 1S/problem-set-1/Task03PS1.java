import java.util.Arrays;
import java.util.Scanner;
public class Task03PS1 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println(sqrt(n));
	}

	static double sqrt(double n) {

		double x = n;
		double y = 1;
		double e = (double) 0.000001; 
		while(x - y > e)
		{
			x = (x + y)/2;
			y = n/x;
		}
		return x;
	}
}