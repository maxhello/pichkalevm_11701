import java.util.Scanner;

public class Task04PS1 {
	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();

		int buf = n;
		for(int i = 2; i <= n; i++) {
			while(buf % i == 0 && buf > 0) {
				System.out.print(i);
				System.out.print(" ");
				buf = buf / i;
			}
			buf = n;
		}
	}
}
