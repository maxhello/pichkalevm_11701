import java.util.Scanner;

public class Task07PS1 {

    public static boolean checking(int i, int j, double radius1, double radius2, int r) {
        return (((i - radius1) * (i - radius1) + (j - radius1) * (j - radius1)) <= (r * r) / 4)
                || (((i - radius2) * (i - radius2) + (j - radius2) * (j - radius2)) > (r * r) / 4 & (i <= j));
    }
    public static void main(String[] args) {
    	
    	Scanner sc = new Scanner(System.in);
    	int r = sc.nextInt();
    	double radius1 = 0.25 * r * (4 - Math.PI);
    	double radius2 = 0.25 * r * (4 + Math.PI);
    	int k = r*2;
    	for (int i = 0; i <= k; i++) {
    		for (int j = 0; j <= k; j++) {
    			
    			if (((i - r) * (i - r) + (j - r) * (j - r)) > r * r) {
    				System.out.print(".");
    			} else if (checking(i, j, radius1, radius2, r)) {
    				System.out.print("0");
    			} else {
    				System.out.print("*");
    			}
    		}
    		System.out.println("");
    	}
    }
}