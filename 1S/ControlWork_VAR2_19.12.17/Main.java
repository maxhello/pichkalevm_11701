
public class Main {
	public static void main(String args[]) {
		Object[] array = new Object[2];
		array[0]= new Sphere(4);
		array[1]= new Cylinder(3, 4);

		for(int i=0; i<array.length; i++) {
			if(array[i] instanceof Sphere) {

				Sphere sph = (Sphere) array[i];
				System.out.println("area of a sphere: " + sph.getS()+ " value of a sphere: "+ sph.getV());

			} else if(array[i] instanceof Cylinder) {

				Cylinder cyl = (Cylinder) array[i];
				System.out.println("area of a cylinder: " + cyl.getS()+ " value of a cylinder: "+ cyl.getV());

			}
		}
	}
}
