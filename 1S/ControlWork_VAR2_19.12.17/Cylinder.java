
public class Cylinder implements Volumeable {

	private double  pi = 3.14;
	private double r;
	private double h;
	Cylinder(double r, double h){
		this.r = r;
		this.h = h;
	}
	@Override
	public double getS() {
		return 2.0*pi*r*h+2.0*pi*r*r;
	
	}

	@Override
	public double getV() {
		return pi*r*r*h;
	}

}
