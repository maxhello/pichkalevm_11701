
public class Sphere implements Volumeable {

	private double  pi = 3.14;
	private double r;
	
	Sphere(double r){
		this.r = r;
	}
	@Override
	public double getS() {
		return 4.0*pi*r*r;
	
	}

	@Override
	public double getV() {
		return 1.0/3*pi*r*r*r;
	}

}
