
public interface Volumeable {
 double getS();
 double getV();
}
