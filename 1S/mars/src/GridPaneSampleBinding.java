

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GridPaneSampleBinding extends Application {

	//Declaring a JavaFX property
	private StringProperty message = new SimpleStringProperty();
	
	
	public void start(Stage primaryStage) {
		
		Label userIdLbl = new Label("Your Text:");
		TextField text = new TextField();
		Label userPwdLbl = new Label("Your comment:");
		PasswordField userPwdTxt = new PasswordField();
		Button signInBtn = new Button ("Commit");
		Button exitBtn = new Button ("Exit");
		Hyperlink forgotU = new Hyperlink("Forgot yourself");
		
		// A label to display messages using binding
		Label messageLbl = new Label();
		// binding the StringProperty to a GUI component
		messageLbl.textProperty().bind(message);
		
		GridPane root = new GridPane();
		root.setVgap(20);
		root.setPadding(new Insets(10));
		root.setAlignment(Pos.CENTER);
		
		// Using static methods for setting node constraints 
		GridPane.setConstraints(userIdLbl, 0, 0);
		GridPane.setConstraints(text, 1, 0);
		GridPane.setConstraints(userPwdLbl, 0, 1);
		GridPane.setConstraints(userPwdTxt, 1, 1);
		GridPane.setConstraints(signInBtn, 0, 2);
		
		//Cancel button: span 1, right aligned
		GridPane.setConstraints(exitBtn, 1,2, 1, 1, HPos.RIGHT, VPos.CENTER);
		GridPane.setConstraints(forgotU, 0, 3,2,1);
		
		// Message label: span 2
		GridPane.setConstraints(messageLbl, 0,4,2,1);

		root.getChildren().addAll(userIdLbl, text, userPwdLbl, userPwdTxt,
				                 signInBtn, exitBtn, forgotU, messageLbl);
		
		
		// event handlers
		//1. Anonymous class 
		signInBtn.setOnAction(e -> {
			   		message.set("Commit clicked."); 
			   	String s = text.getText();
			   	MessageSenderServlet zzz = new MessageSenderServlet();
			   		zzz.xxx= s;
			   		System.out.println(s);
		});
		
		// lambda expression
		exitBtn.setOnAction(e -> Platform.exit());
		
		
		// method reference
		forgotU.setOnAction(this::forgotPwdHandler);
		
		// Show the window
		Scene scene = new Scene(root,500,500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mars Project");
		primaryStage.show();

	}
	
	private void forgotPwdHandler(ActionEvent evt){
		message.set("Forgot myself clicked");
	}

	public static void main(String[] args) {
		launch(args);
	}
}
