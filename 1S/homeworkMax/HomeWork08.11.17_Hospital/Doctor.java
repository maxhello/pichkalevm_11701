import java.io.PrintWriter;

public class Doctor {// output all records  of his patients
	 String name;
     int numberOfPatients=0;;
	 Patient[] patients = new Patient[100];;
	 String record[];
	
	public Doctor(String name){
		this.name = name;
	}
	
	public void addPatient(Patient patient) {
		patients[numberOfPatients] = patient;
		numberOfPatients++;
		System.out.println("Patient " + patient.getFio() +  " added to doctor");
	}
	public Patient [] getPatients() {
		Patient [] realPatients = new Patient[numberOfPatients];	
		for (int i = 0; i < numberOfPatients; i++) {
			realPatients[i] = patients[i];
		}
		return realPatients;
	}
}

class DoctorSingleton {
	private static Doctor doctor= new Doctor("Freid");
	private static Doctor doctor2= new Doctor("Zigmund");
	
	public static Doctor getDoctor(String name) {
		if(name.equals("Freid")) {
			return doctor;
		}else if(name.equals("Zigmund")) return doctor2;
		return doctor;
	}
	
}