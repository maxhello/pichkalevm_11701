import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

class HospitalSingleton {
	private static Hospital hospital;
	public static Hospital getHospital() {
		if (hospital == null) {
			hospital = App.loadHospital("hospital.txt");
		}
		return hospital;
	}
}

public class App {	
	public static void main(String [] args) {
		
		App app = new App();
		app.init();
		app.start();
		app.save();
		
	}
	
	public void init() {        // your initialization( who are you). Record to the doctor
		//loadPatients();
	}
	
	public static Hospital loadHospital(String filename) {
		Hospital hospital;
		try {
			Scanner sc = new Scanner(new File(filename));
			hospital = new Hospital(
				sc.nextLine(),
				sc.nextLine(),
				Integer.parseInt(sc.nextLine()),
				Integer.parseInt(sc.nextLine())
			);
		}
		catch (FileNotFoundException e) {
			hospital = new Hospital("Dummyy Hospital", "Bern", 100, 0);
		}
		return hospital;
	}
	
	public void save() {
		try {
			PrintWriter pw = new PrintWriter("patients.txt");
			PrintWriter pwFreid = new PrintWriter("patientsOFreid.txt");
			PrintWriter pwZigmund = new PrintWriter("patientsOZigmund.txt");
			Patient [] patients = HospitalSingleton.getHospital().getPatients();
			for (Patient patient: patients) {
				
				patient.dumpToWriter(pw);

				
			}
			Patient [] patientsF = DoctorSingleton.getDoctor("Freid").getPatients();
            for (Patient patient: patientsF) {
			
		          
				patient.dumpToWriter(pwFreid);
			}
        	Patient [] patientsZ = DoctorSingleton.getDoctor("Zigmund").getPatients();
            for (Patient patient: patientsZ) {
			
		          
				patient.dumpToWriter(pwZigmund);
			}
            pwZigmund.close();
            pwFreid.close();
			pw.close();
		} 
		catch (FileNotFoundException e) {
			
		}
	}
	
	public void start() {
		
		Scanner sc = new Scanner(System.in);
		Hospital hospital = HospitalSingleton.getHospital();
		Doctor doctor = null;
		
		while (true) {
			System.out.println("plz, write 1)exit; 2) new p; 3) show all p");
			String cmnd = sc.nextLine();
			
			if (cmnd.equals("exit")) {
				break;
			}
			
			else if (cmnd.equals("new p")) {
				System.out.println("Enter name of Patient");
				String name = sc.nextLine();
				System.out.println("Enter patient's date Of Birth");
				String date = sc.nextLine();
				System.out.println("Enter sex of Patient");
				String sex = sc.nextLine();
				System.out.println("do you want to doctor?");
				String flag="";
				String record;
				String nameD = "";
				
				while(!flag.equals("yes") & !flag.equals("yes")) {
					System.out.println("plz enter yes or no");
				
					 flag = sc.nextLine();
				if(flag.equals("yes")) {
					
					
				 System.out.println(" Chose youe doctor: Freid or Zigmund?" );
				 
					
					while(!nameD.equals("Freid") & !nameD.equals("Zigmund")) {
						System.out.println("plz enter Freid or Zigmund");
						nameD = sc.nextLine();
					if(nameD.equals("Freid")) {
						
						 doctor = DoctorSingleton.getDoctor("Freid");
					}else 
						 if(nameD.equals("Zigmund")) {
							  doctor = DoctorSingleton.getDoctor("Zigmund");
						 }
					}
				 
					System.out.println("Plz, write your desired time.");
					
					 record = sc.nextLine();
					 
					 hospital.addPatient(new Patient(name, date, sex, record));
					 doctor.addPatient(new Patient(name, date, sex, record));
				}else if (flag.equals("no")){
					hospital.addPatient(new Patient(name, date, sex));
				}
				}
				
				
			}
			else if (cmnd.equals("show all p")) {
				System.out.println(Arrays.toString(hospital.getPatients()));
			}
			
		}
		
	}
	
	
}