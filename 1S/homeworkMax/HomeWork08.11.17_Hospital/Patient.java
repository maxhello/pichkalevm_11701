import java.io.PrintWriter;
import java.util.Date;

public class Patient {

	private String fio;
	private String dateOfBirth;
	private Address address;
	private String passportInfo;
	private String gender;
	private String [] anamnesis = new String[20];
	private int nAnamnesisNotes = 0;
	private String recordToDoctor;
	
	public void setDoB(String dateOfBirth) {  // date of birth
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getDoB() {
		return this.dateOfBirth;
	}
	
	public void setSex(String sex) {
		gender = sex;
	}
	public String getSex() {
		return gender;
	}
	public String getFio() {
		return this.fio;
	}
	
	public void setFio(String fio) {
		this.fio = fio;
	}
	public String getRecord() {
		return recordToDoctor;
	}
	
	public void setRecord(String record) {
		recordToDoctor = record;
	}

	
	public Patient(String fio, String dateOfBirth, String gender,  String record ) {
		setFio(fio);
		setDoB(dateOfBirth);
		setSex(gender);
		setRecord(record);
		
		System.out.println("Patient " + fio + " created");
	}
	
	
	public Patient(String fio, String dateOfBirth, String gender) {
		setFio(fio);
		setDoB(dateOfBirth);
		setSex(gender);
		
		System.out.println("Patient " + fio + " created");
	}
	
	
	public String toString() {
		return "{patient: {name: " + fio + "}}";
	}
	
	public void dumpToWriter(PrintWriter pw) {
		pw.println("#PATIENT");
		pw.println(fio);
		pw.println(dateOfBirth == null ? "" : dateOfBirth);
		pw.println(gender == "" ? "" : gender);
		pw.println(recordToDoctor == "" ? "" : recordToDoctor);
		pw.println(address == null ? "" : address);
		pw.println(passportInfo == null ? "" : passportInfo);
		pw.println(nAnamnesisNotes);
		for (int i = 0; i < nAnamnesisNotes; i++) {
			pw.println(anamnesis[i]);
		}
	}
}