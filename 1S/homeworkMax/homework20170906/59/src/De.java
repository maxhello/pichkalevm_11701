import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class De {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Double x=0.0,y=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if ((y>=-2*x-1)&&(y<=-2*x+1)&&(y<=2*x+1)&&
                (y>=2*x-1)&&(-2*x>=y-1)&&(-2*x<=y+1)&&(2*x<=y+1)&&(2*x>=y-1))
            System.out.println("Yes, the point belongs to the rhombus ");
        else System.out.println("Sorry, the point doesnt belong to the rhombus ");
    }
}
