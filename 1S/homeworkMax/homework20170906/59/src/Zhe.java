import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Zhe {
    public static  void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Double x=0.0,y=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if ((y>=-1)&&(y<=2*x+2)&&(y<=-2*x+2)&&(2*x<=-y+2)&&(2*x>=y-2))
            System.out.println("Yes, the point belongs to the figure ");
        else System.out.println("Sorry, the point doesnt belong to the triangle ");
        System.out.println(x);
        System.out.println(y);
    }
}