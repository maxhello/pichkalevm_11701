import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class E {
    final static public double e= 2.71828;
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double x=0.0,y=0.0,u=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if ((y>=x*x)&&(y<=Math.pow(e,x))&&(y<=Math.pow(e,-x)))
            System.out.println("belong, u == "+ (x+y));
        else System.out.println("doesnt belong, u == "+ (x-y));

    }
}