import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double x=0.0,y=0.0,u=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if (((x*x+y*y)<=4)&&((x*x+y*y)>=1)) System.out.println("u == 0 ");
        else System.out.println("u = "+ x);
    }
}
