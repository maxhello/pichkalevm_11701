import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Be {
    public static void main(String[] args) {
        long n=0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try{
            n=Long.parseLong(reader.readLine());
        }
        catch(Exception egege){
            System.out.println("Somthing bad");
        }

        Factorial f = new Factorial();
        System.out.println(f.fact(n));
    }

}
class Factorial {

    long fact(long n) {
        long result;

        if (n == 1)
            return 1;
        result = fact(n - 1) * n;
        return result;
    }
}