import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ge {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Double x=0.0,y=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if ((y>=-x-1)&&(y<=x+1)&&(y<=-x+1)&&
                (y>=x-1)&&(x>=-y-1)&&(x<=y+1)&&(x>=y-1)&&(x<=-y+1))
            System.out.println("Yes, the point belongs to the square ");
        else System.out.println("Sorry, the point doesnt belong to the square ");
    }
}
