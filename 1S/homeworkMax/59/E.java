import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class E {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double x=0.0,y=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        if (((x>=0)&(x*x+y*y)<=1)|(((y<=0.50*x+1)&&(x>=(y-1)/0.50)&&(x<=0))&&
                ((y>=-0.5*x-1)&&(x>=(-y-1.0)/0.5)&&(x<=0))))
            System.out.println("Yes, the point belongs to the figure ");
        else System.out.println("Sorry, the point doesnt belong to the figure ");
        System.out.println(x);
        System.out.println(y);
    }
}