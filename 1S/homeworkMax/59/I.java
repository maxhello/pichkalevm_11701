import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class I {
    public static  void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Double x=0.0,y=0.0;
        try {
            x = Double.parseDouble(reader.readLine());
            y = Double.parseDouble(reader.readLine());
        }
        catch(IOException e){
            System.out.println("Somthing wrong!!!");
        }
        Double F,Z,B,N,M;
        F=1/3*x-1/3;

        if (((y>=1/3.0*x-1.0/3.0)&&(x<=3.0*y+1)&&(y<=2.0*x+3)&&(x>=(y-3)/2.0)&&(y<=0))||
                ((y<=2.0*x+3)&&(x>=(y-3.0)/2.0)&&(y<=-x)&&(x<=-y)&&(y>0)))
            System.out.println("Yes, the point belongs to the figure ");
        else System.out.println("Sorry, the point doesnt belong to the figure ");
        System.out.println(x);
        System.out.println(y);

    }
}