import java.io.*;
import java.util.*;

public class Genetic {
	private static Random rand;
	private static BufferedReader read;
	public static void main(String args[]) throws IOException{

		File file = new File("Objects");

		read = new BufferedReader(new FileReader(file));

		 int size = 6;
		int capacity = 3*size/2;
		 rand = new Random(System.currentTimeMillis());
		
	
		String[] object = new String[capacity];

		int i = 0;
		
		while ((object[i] = read.readLine()) != null) {
			i++;
  }

int generation = 0;
int finePop = 0;
while ((finePop < capacity/1.5)&&(generation < capacity)) {
	for (int k = 1; k <= size/2; k++) {
				object[size-1+i] = "/////";
		for (int j = 0; j < 5; j++) {
					object[size-1+i] = replacement(object[size-1+k], j, object[2*k-1-rand.nextInt(2)].charAt(j));
	}
	}

	for (int k = 0; k < capacity; k++) {
		System.out.print(object[i] + "=" + quality(object[k])+ "   ");
		if(i == capacity - 1)System.out.println();
		}
			

	finePop = 0;
	for (int k = 0; k < capacity; k++) {
		if (quality(object[k]) > 0) {
			if (quality(object[k]) == 1) {
				finePop++;
			}
			int j = k;
			while ((j > 0) && (quality(object[j-1]) < quality(object[j]))) {
			String t = object[j];
			object[j] = object[j-1];
			object[j-1] = t;
			j--;
					}
				}
			}
			for (int k = 0; k < size; k++) {
				System.out.print(object[k] + "=" + quality(object[k])+ "   ");
			}
			System.out.println();
			generation++;
		}
	}
	
	public static String replacement(String x, int position, char z) {
		return x.substring(0, position) + z + x.substring(position + 1);
	}
	
	public static double quality(String object) {
		double qua = 0;
		for (int i = 1; i < 5; i++) {
			if (Math.abs(object.charAt(i)-object.charAt(i-1)) > 5) {
				qua += 0.25;
			}
		}
		return qua;
	}
}