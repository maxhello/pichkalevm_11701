import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Game {
	static int choosenShip;
	static int credits = 5000000; 
	private final static int LIMIT = 500000;
	static boolean flag = true;
	static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
	static ArrayList fieldA = new ArrayList();
	static ArrayList fieldB = new ArrayList();
	static ArrayList fieldC = new ArrayList();
	static ArrayList fieldD = new ArrayList();
	static ArrayList fieldE = new ArrayList();
	
	public static void main(String args[]) {
		Information info = new Information();
		Thread th = new Thread(info, "info");
		
		while(flag) {
			
			fieldE.addAll(fieldD);		
			fieldD.clear();
			fieldD.addAll(fieldC);
			fieldC.clear();
			fieldC.addAll(fieldB);
			fieldB.clear();
			fieldB.addAll(fieldA);
			fieldA.clear();
			
			createShip();
			space();
//			th.start();
		    
		}
	}

//	public int damageA(){
//		int damageA;
//		
//	}
//	public int damageB(){
//		int damageB;
//		
//	}
//	public int damageC(){
//		int damageC;
//		
//	}
//	public int damageD(){
//		int damageD;
//		
//	}
//	public int damageE(){
//		int damageE;
//		
//	}
	
	public static void space( ){
	
		
		if(!fieldB.isEmpty()) {
			int hcC = 0;
			int hcC_HP = 0;
			int lfC = 0;
			int lfC_HP = 0;
			int hcC_dmg = 0;
			int lfC_dmg = 0;
		    int sum_damage = 0;
		    int sum_hp = 0;
			for(int i=0; i<fieldB.size(); i++ ) {
				Object currentShip = fieldB.get(i);
				
				if (currentShip instanceof HC){
					HC hc= (HC)fieldB.get(i);
					hcC++;                    // count of heavy ship
					hcC_HP=hcC_HP+ hc.structure;
					hcC_dmg=hcC_dmg + hc.damage; // take the damage
					}
					else if (currentShip instanceof LF){
						LF lf = (LF) fieldB.get(i);
					lfC++;                           // count of light ship
					lfC_HP = lfC_HP+ lf.structure;
					lfC_dmg=lfC_dmg+lf.damage;    // take the damage
					}

			}
             sum_damage = lfC_dmg + hcC_dmg; 
             sum_hp=lfC_HP+ hcC_HP;
			System.out.println("field B has: " +"number of Heavy Cruiser, Damage, HP: "+ hcC+", "+ hcC_dmg+ ", " +hcC_HP + "\n             number of LightFighters, Damage, HP: "+ lfC+", "+lfC_dmg+", "+lfC_HP+ "\n B Cell's damage, hp: "+ sum_damage+ ", " + sum_hp);
		    
		}
		
		if(!fieldC.isEmpty()) {
			int hcC = 0;
			int hcC_HP = 0;
			int lfC = 0;
			int lfC_HP = 0;
			int hcC_dmg = 0;
			int lfC_dmg = 0;
		    int sum_damage = 0;
		    int sum_hp = 0;
		    int eDamage=73;
		    int wasDestoyed_HC = 0;
		    int wasDestoyed_LF = 0;
			for(int i=0; i<fieldC.size(); i++ ) {
				Object currentShip = fieldC.get(i);
				
				
				
				
				
				
				
				
				if (currentShip instanceof HC){
					
					HC hc= (HC)fieldC.get(i);
					int startHP=hc.structure;
					hc.structure= hc.structure - eDamage;
					
					if(hc.structure <= 0) {            // destroy ship, change damage
						eDamage = eDamage-startHP;
						fieldC.remove(i);
						i--;
						wasDestoyed_HC++;
					} else {
						eDamage = 0;
						hcC++;
					}
						
						if(hc.structure<=0) {
							
						} else hcC_HP=hcC_HP+ hc.structure;
						
					hcC_dmg=hcC_dmg + hc.damage; // take the damage
					
	
					
					
					
				}else if (currentShip instanceof LF){
						LF lf = (LF) fieldC.get(i);
						int startHP = lf.structure;
					                          // count of light ship
					lf.structure= lf.structure - eDamage;
					if(lf.structure <= 0) {            // destroy ship, change damage
						eDamage = eDamage-startHP;
						fieldC.remove(i);
						i--;
						wasDestoyed_LF++;
					} else {
						eDamage = 0;
						lfC++;
					}
					
					if(lf.structure<=0) {
						
					} else lfC_HP=lfC_HP+ lf.structure;
					
					
					
					
					lfC_dmg=lfC_dmg+lf.damage;          // take the damage
					}

			}
             sum_damage = lfC_dmg + hcC_dmg; 
             sum_hp=lfC_HP+ hcC_HP;
            
             System.out.println("field C has: " +"number of Heavy Cruiser, Damage, HP: "+hcC +", "+ hcC_dmg+ ", " +hcC_HP + "\n             number of LightFighters, Damage, HP: "+ lfC+", "+lfC_dmg+", "+lfC_HP+ "\n C Cell's damage, hp: "+ sum_damage+ ", " + sum_hp);
		    
		}
		if(!fieldD.isEmpty()) {
			int hcC = 0;
			int hcC_HP = 0;
			int lfC = 0;
			int lfC_HP = 0;
			int hcC_dmg = 0;
			int lfC_dmg = 0;
		    int sum_damage = 0;
		    int sum_hp = 0;
		    
			for(int i=0; i<fieldD.size(); i++ ) {
				Object currentShip = fieldD.get(i);
				
				if (currentShip instanceof HC){
					HC hc= (HC)fieldD.get(i);
					hcC++;                    // count of heavy ship
					hcC_HP=hcC_HP+ hc.structure;
					hcC_dmg=hcC_dmg + hc.damage; // take the damage
					}
					else if (currentShip instanceof LF){
						LF lf = (LF) fieldD.get(i);
					lfC++;                           // count of light ship
					lfC_HP = lfC_HP+ lf.structure;
					lfC_dmg=lfC_dmg+lf.damage;    // take the damage
					}

			}
             sum_damage = lfC_dmg + hcC_dmg; 
             sum_hp=lfC_HP+ hcC_HP;
             System.out.println("field D has: " +"number of Heavy Cruiser, Damage, HP: "+hcC +", "+ hcC_dmg+ ", " +hcC_HP + "\n             number of LightFighters, Damage, HP: "+ lfC+", "+lfC_dmg+", "+lfC_HP+ "\n D Cell's damage, hp: "+ sum_damage+ ", " + sum_hp);
		    
		}
		
		if(!fieldE.isEmpty()) {
			int hcC = 0;
			int hcC_HP = 0;
			int lfC = 0;
			int lfC_HP = 0;
			int hcC_dmg = 0;
			int lfC_dmg = 0;
		    int sum_damage = 0;
		    int sum_hp = 0;
		    
			for(int i=0; i<fieldE.size(); i++ ) {
				Object currentShip = fieldE.get(i);
				
				if (currentShip instanceof HC){
					HC hc= (HC)fieldE.get(i);
					hcC++;                    // count of heavy ship
					hcC_HP=hcC_HP+ hc.structure;
					hcC_dmg=hcC_dmg + hc.damage; // take the damage
					}
					else if (currentShip instanceof LF){
						LF lf = (LF) fieldE.get(i);
					lfC++;                           // count of light ship
					lfC_HP = lfC_HP+ lf.structure;
					lfC_dmg=lfC_dmg+lf.damage;    // take the damage
					}

			}
             sum_damage = lfC_dmg + hcC_dmg; 
             sum_hp=lfC_HP+ hcC_HP;
             System.out.println("field E has: " +"number of Heavy Cruiser, Damage, HP: "+hcC +", "+ hcC_dmg+ ", " +hcC_HP + "\n             number of LightFighters, Damage, HP: "+ lfC+", "+lfC_dmg+", "+lfC_HP+ "\n E Cell's damage, hp: "+ sum_damage+ ", " + sum_hp);
		    
		}
	}
	
	static void createShip(){
		int purse = 0;
		System.out.println("choose ships which you want, 1-hc, 2-lk, 0-exit from the shop... ");
		
		while(purse < LIMIT) {
			
	      try {
			 choosenShip= Integer.parseInt(read.readLine());
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		if((choosenShip == 1) && (purse==0) ) {
			HC hc = new HC();
			fieldA.add(hc);
			credits=credits - 500000;
			purse = purse + 500000;
		} else if(choosenShip == 1 && purse!=0) System.out.println("u cant afford it youself");
		if(choosenShip == 2) {
			LF lf = new LF();
			fieldA.add(lf);
			credits=credits - 50000;
			purse= purse + 50000;
		}
		if(choosenShip == 0 )break;
		System.out.println("Your purse: " + purse);
		}

	}
	
}
class Information implements Runnable {
	Game game = new Game();
	public void run() {
		
		System.out.println("current moneys: " + game.credits );
		
		
	}
}

