import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;







public class ClientGamerZ {
	
	static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
	static String ip;
	 static int port;
	 static Socket socket;
	static InputStream sin;
	static OutputStream sout;
	static DataInputStream in;
	static DataOutputStream out;
	static boolean flag=false;
	
	
	
	private static char[][] board= {{'-','-','-'},{'-','-','-'},{'-','-','-'}};


	public static boolean canMove() {    // check the board, is there some place?
	    boolean p = false;
	    for (int i = 0; i < 3; i++) {
	        for (int j = 0; j < 3; j++) {
	            if (board[i][j] == '-') {
	                p = true;
	                break;
	            }
	        }
	        if (p = true) {
	            break;
	        }
	    }
	    return p;
	}

	private static void cleanTheBoardAndCheck(){    // clean the board if 1, else if 0 do nothing
		int yesOrNo=-1;                             // вызываем при победе или при заполнении поля
		System.out.println(" Do you want to replay this game? ");

		try {
			
			
			System.out.println(" If yes, input 1, if no, input 0. Thanks ");
			
				yesOrNo = Integer.parseInt(read.readLine());
			
			
				while((yesOrNo>1)||(yesOrNo<0)) {
					System.out.println(" If yes, input 1, if no, input 0. Thanks ");
				yesOrNo = Integer.parseInt(read.readLine());}
			   } catch (NumberFormatException | IOException e) {
				e.printStackTrace();
			}
		
		
		//  clean the board
		if(yesOrNo==1)  for (int i = 0; i < 3; i++) 
			for (int j = 0; j < 3; j++) board[i][j] ='-';
		if(yesOrNo==0) System.out.println(" Push the terminate. Thanks ");
		
			
				
	}


	private static void currentBoard(){               // show the board
		{
			System.out.println("— — — — — — -");
			for (int i = 0; i < 3; i++)
			{
				System.out.print("| ");
				for (int j = 0; j < 3; j++)
				{
					System.out.print(board[i][j] + " | ");
				}
				System.out.println();
				System.out.println("— — — — — — -");
			}
		}
	}

	
	public static  boolean isWinner() {                                      // is there a winner?
		return (checkRows() || checkColumns() || checkDiagonals());
	}

	private static boolean checkRows(){                         // isW
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[i][0], board[i][1], board[i][2]) == true)
			{
				return true;
			}
		}
		return false;

	}
	
	
	

	private static boolean checkColumns(){                       // isW
		for (int i = 0; i < 3; i++) {
			if(checkRowCol(board[0][i], board[1][i], board[2][i]) == true)
			{
				return true;
			}
		}
		return false;
	}
	
	
	
	private static boolean checkDiagonals() {                          // isW
		return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true) 
				|| (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
	}

	private static boolean checkRowCol(char c1, char c2, char c3) {           // isW
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}
	
	
	
	
	public static boolean placeMarkAndCheck(int row, int col) {                  // put the chosen sign
		
		if ((row >= 0) && (row<=2)&&(col>=0)&&(col <= 2)&&(board[row][col] == '-')&&(flag==false)) {
			board[row][col] = 'X';
			flag=true;
			if( isWinner()) {
				currentBoard();
				System.out.println(" YOU LOST THIS GAME. \n \t Сongratulations. ");
			
			cleanTheBoardAndCheck();
			
			} else if(!canMove()) {
				currentBoard();
				System.out.println(" Tie. You was great. Thanks ");
				cleanTheBoardAndCheck();
				
			}
				
				
		        return true;
		        } else
		        	if ((row >= 0) && (row <= 2 )&&( col >= 0 )&&(col <= 2)&&( board [row] [col] == '-' )&&( flag == true )) {
		        		board [row] [col] = 'O';
		        		if( isWinner()) { 
		        			currentBoard();
		        			System.out.println("YOU WON THIS GAME"); 
		        			cleanTheBoardAndCheck();
		        			
		        		}else if(!canMove()) {
		        			currentBoard();
		        			System.out.println(" Tie. You was great. Thanks ");
		        			cleanTheBoardAndCheck();
		        			
		        		}
		        		
		        		flag = false;
		        	        return true;
		        	}
		        	
		      return false;
		    }

	
	// static void putTheEnemyLetter(int number1, int number2 ) {                   // put the enemy sign             
	//	
	//		 board[number1][number2]='X';
	// }

	 private static void makeConnection(){
		 
		try {
			
			System.out.println("Please, write your ip");
			ip = read.readLine();
		
		 System.out.println("Please, write your port");
		 port = Integer.parseInt(read.readLine());
		 InetAddress ipAddress = InetAddress.getByName(ip); 
         System.out.println(" IP address " + ip + " and port " + port );
          socket = new Socket(ipAddress, port);
          
      System.out.println("succesful connected");
      System.out.println();

      sin = socket.getInputStream();
      sout = socket.getOutputStream();


      in = new DataInputStream(sin);
       out = new DataOutputStream(sout);
       
} catch (IOException e) {
			
			e.printStackTrace();
		}
	 }
	 
   
		 
	public static void main(String[] args) {
	     makeConnection();
		       try {      
		        while(true){
		        	currentBoard();
		        	int EnemyNumber1=in.readInt();
		        	int EnemyNumber2=in.readInt();		       
		        	//putTheEnemyLetter(EnemyNumber1,EnemyNumber2);
		        	placeMarkAndCheck(EnemyNumber1,EnemyNumber2);
		        	currentBoard();		        
		        	int number1=-1;
		        	int number2=-1;
		        //	System.out.println("Make your choice.");
		       // 	while((number1<=0)&&(number1>=3)&&(number2>=3)&&(number2<=0)) {
		        	while(!placeMarkAndCheck(number1, number2)) {
		        		 System.out.println("Plz, write your coordinates from 0 to 2, x2!");
		        	 number1=Integer.parseInt(read.readLine());
		        	 number2=Integer.parseInt(read.readLine());
		        	}
		        	
		        	currentBoard();

		           out.writeInt(number1); 
		           out.writeInt(number2); 
		           out.flush();
		        
				}
		      } catch(Exception x) { x.printStackTrace(); }
		}
	}








