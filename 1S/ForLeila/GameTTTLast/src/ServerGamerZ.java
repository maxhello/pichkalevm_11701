import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;






public class ServerGamerZ {
	
	static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
	 static int port;
	 static Socket socket;
	static InputStream sin;
	static OutputStream sout;
	static DataInputStream in;
	static DataOutputStream out;
	static boolean flag=false;
	 static ServerSocket ss;
	
	 
	
	private static char[][] board= {{'-','-','-'},{'-','-','-'},{'-','-','-'}};

	private static void artBrain() {           
		int x = (int) (Math.random() * 3), y = (int) (Math.random() * 3); 
		while (board[x][y] == 'O' || board[x][y] == 'X') {
			x = (int) (Math.random() * 3);
			y = (int) (Math.random() * 3);
		}
		board[x][y] = '0';
		if(isWinner()) {
			currentBoard();
			System.out.println(" Bot won. Congratulations ");
			cleanTheBoardAndCheck();
		} else if(!canMove()) {
			System.out.println(" Tie. Congratulations ");
			cleanTheBoardAndCheck();
		}
		flag=false;
	}
	
	
	
	
	private static boolean  canMove() {
	    for (int i = 0; i < 3; i++)
	        for (int p=0; p < 3; p++)
	            if(board [i][p]=='-')
	                return true;

	    return false;
	}

	
	
	
	private static void cleanTheBoardAndCheck(){    // clean the board if 1, else if 0 do nothing
		int yesOrNo=-1;                            
		System.out.println(" Do you want to replay this game?" );
		
		
			while((yesOrNo>1)||(yesOrNo<0)) {
				try {
				System.out.println(" If yes, input 1, if no, input 0. Thanks ");
			yesOrNo = Integer.parseInt(read.readLine());
		   } catch (NumberFormatException | IOException e) {
			   System.out.println(" Please, only 1 or 0. ");
		}
}
		
		
		//  clean the board
		if(yesOrNo==1)  for (int i = 0; i < 3; i++) 
			for (int j = 0; j < 3; j++) board[i][j] ='-';
		if(yesOrNo==0) { System.out.println(" Pushed the termination. Thanks ");
		System.exit(0);
		}
		
			
				
	}


	private static void currentBoard(){               // show the board
		{
			System.out.println("-------------");
			for (int i = 0; i < 3; i++)
			{
				System.out.print("| ");
				for (int j = 0; j < 3; j++)
				{
					System.out.print(board[i][j] + " | ");
				}
				System.out.println();
				System.out.println("-------------");
			}
		}
	}

	
	public static  boolean isWinner() {                                      // is there a winner?
		return (checkRows() || checkColumns() || checkDiagonals());
	}

	private static boolean checkRows(){                         // isW
		for (int i = 0; i < 3; i++) {
			if (checkRowCol(board[i][0], board[i][1], board[i][2]) == true)
			{
				return true;
			}
		}
		return false;

	}
	
	
	

	private static boolean checkColumns(){                       // isW
		for (int i = 0; i < 3; i++) {
			if(checkRowCol(board[0][i], board[1][i], board[2][i]) == true)
			{
				return true;
			}
		}
		return false;
	}
	
	
	
	private static boolean checkDiagonals() {                          // isW
		return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true) 
				|| (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
	}

	private static boolean checkRowCol(char c1, char c2, char c3) {           // isW
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}
	
	
	
	
	public static boolean placeMarkAndCheck(int row, int col) {   
	
	if ((row >= 0) && (row<=2)&&(col>=0)&&(col <= 2)&&(board[row][col] == '-')&&(flag==false)) {
		board[row][col] = 'X';
		flag=true;
		if( isWinner()) {currentBoard();
			System.out.println(" YOU WON THIS GAME. \n \t Congratulations. ");
		
		cleanTheBoardAndCheck();
		
		}else if(!canMove()) {
			currentBoard();
			System.out.println(" Tie. You was great. Thanks ");
			cleanTheBoardAndCheck();
			
		}
			
			
	        return true;
	        } else
	        	if ((row >= 0) && (row <= 2 )&&( col >= 0 )&&(col <= 2)&&( board [row] [col] == '-' )&&( flag == true )) {
	        		board [row] [col] = 'O';
	        		if( isWinner()) { 
	        			currentBoard();
	        			System.out.println("YOU LOST THIS GAME"); 
	        			cleanTheBoardAndCheck();
	        			
	        		}else if(!canMove()) {
	        			currentBoard();
	        			System.out.println(" Tie. You was great. Thanks ");
	        			cleanTheBoardAndCheck();
	        			
	        		}
	        		
	        		flag = false;
	        	        return true;
	        	}
	        	
	      return false;
	    }

	
	// static void putTheEnemyLetter(int number1, int number2 ) {                   // put the enemy sign             
	//	
	//		 board[number1][number2]='X';
	// }

	 private static void makeConnection(){
		 
		try {
			
			System.out.println("Please, write your port");
			port = Integer.parseInt(read.readLine());
		
			  ss = new ServerSocket(port); 
	         System.out.println("Waiting for a client...!!!");

	          socket = ss.accept(); 
	          
	         System.out.println("Opponent is here!");
	         System.out.println();
	         
      sin = socket.getInputStream();
      sout = socket.getOutputStream();


      in = new DataInputStream(sin);
       out = new DataOutputStream(sout);
       
} catch (IOException e) {
			
	System.out.println("Your opponent run away. Thanks. ");
		}
	 }
	 
	private static boolean launcer(){
		int choice=-1;
		while(choice>1|| choice<0) {
			try {
			System.out.println("Do you want to play with another player? Or with bot? \n If first, input 1. If second, input 0.");
			
				choice = Integer.parseInt(read.readLine());
			} catch (NumberFormatException | IOException e) {
				System.out.println(" Only 1 or 0. Thanks ");
			}
		}
			
			 
		if(choice==1)return true;		
		return false;
		 
	 }
   
		 
	public static void main(String[] args) {
		if(launcer()) {       // if man is chosen(if 1)
	     makeConnection();
	     currentBoard();
		     
		        while(true){
		        	int number1 = -1 ;
		        	int number2 = -1 ;
		        	//while((number1<=0)&&(number1>=3)&&(number2>=3)&&(number2<=0)) {
		        	while( !placeMarkAndCheck( number1, number2 )) {
		        		  try {      
		        		 System.out.println(" Plz, write your coordinates from 0 to 2, X2! ");
		        	 number1 = Integer.parseInt(read.readLine());
		        	 number2 = Integer.parseInt(read.readLine());
		        	} catch(Exception x) {System.out.println(" Only 0,1,2. ");} 
		        	}
		        	
		        	currentBoard();
		        	
		        	try {
						out.writeInt(number1);
					
		        	out.writeInt(number2); 
		        	out.flush();
		        	int EnemyNumber1=in.readInt();
		        	int EnemyNumber2=in.readInt();
		        	//putTheEnemyLetter(EnemyNumber1,EnemyNumber2);
		        	
		        	placeMarkAndCheck(EnemyNumber1,EnemyNumber2);
		        	currentBoard();		        

		        	} catch (IOException e) {
		        	     
		        	} 
		        	
		        
				}
		      
		      
		       
		       
		} else {           // if bot is chosen(if 0)
			while(true) {
				
			  currentBoard();
				int number1 = -1 ;
	        	int number2 = -1 ;
	        	//while((number1<=0)&&(number1>=3)&&(number2>=3)&&(number2<=0)) {
	        	while( !placeMarkAndCheck( number1, number2 )) {    
	        		try {
	        		 System.out.println(" Plz, write your coordinates from 0 to 2, X2! ");
	        	 number1 = Integer.parseInt(read.readLine());
	        	 number2 = Integer.parseInt(read.readLine());
	        		}catch(Exception x) {
						System.out.println("Please, only 0,1,2. Be careful. "); 
					}
	        	}
				
	        	 
				artBrain();
				
		
			}
		}
	}
}
