package snippet;

public class Snippet {
	  public void buttonClickHandler(ActionEvent evt){
			 
			Button clickedButton = (Button) evt.getTarget();
		
			String number = clickedButton.getText();
			if(( number != "" ) && (number!= "X") &&(number !="O")) {
				if (isFirstPlayer){
					clickedButton.setText("X");
					isFirstPlayer = false;
					
				} else if(!isFirstPlayer){
					clickedButton.setText("O");
					isFirstPlayer = true;
				}
			
			}
			find3InARow();  // is there a winner?
	   }	
	   
}

