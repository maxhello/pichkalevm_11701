package tictactoe;

import java.net.*;
import java.util.Scanner;
import java.io.*;
public class Server extends Main implements Runnable {
	private boolean yourTurn = false;
	private boolean circle = true;
	
	private Scanner scanner = new Scanner(System.in);
	private String ip = "192.168.0.7";
	private int port = 22222;
	private boolean accepted = false;
	private Socket socket;
	private DataOutputStream dos;
	private DataInputStream dis;
	private ServerSocket serverSocket;
	private Thread thread;
	
	public void run() {
		while(true) {
			if (!accepted) {    // if it is the server
				listenForServerRequest();
			}
			
		}
		// TODO Auto-generated method stub
		
	}
	public Server(){            // Constructor! ������ ������, ������� ������!
		System.out.println("Please input the IP: ");
		ip = scanner.nextLine();
		System.out.println("Please input the port: ");
		port = scanner.nextInt();
		while (port < 1 || port > 65535) {
			System.out.println("The port you entered was invalid, please input another port: ");
			port = scanner.nextInt();
			
		}
		if (!connect()) initializeServer(); // if no any server, create it!

	       
		  thread = new Thread(this, "TicTacToe");
		 thread.start();     // start the threading
	
	}
	private boolean connect() { // try to connect the server
		try {
			socket = new Socket(ip, port);
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
			accepted = true;
		} catch (IOException e) {
			System.out.println("Unable to connect to the address: " + ip + ":" + port + " | Starting a server");
			return false;
		}
		System.out.println("Successfully connected to the server.");
		return true;
	}
	
	private void initializeServer() {  // create the server
		try {
			
			serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		yourTurn = true;
		circle = false;
	}

	
	private void listenForServerRequest() { // if smb connected, change accepted to true
		Socket socket = null;
		try {
			socket = serverSocket.accept();
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
			accepted = true;
			System.out.println("CLIENT HAS REQUESTED TO JOIN, AND WE HAVE ACCEPTED");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
   public static void main(String[] args)    {
    
    
      

   }
   
   
}

