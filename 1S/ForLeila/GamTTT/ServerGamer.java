import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerGamer  {

	static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
	 private static String[] board = {
		        null, null, null,
		        null, null, null,
		        null, null, null};
	
	 public static boolean hasWinner() {  
		 return
				 (board[0] != null && board[0] == board[1] && board[0] == board[2])
				 ||(board[3] != null && board[3] == board[4] && board[3] == board[5])
				 ||(board[6] != null && board[6] == board[7] && board[6] == board[8])
				 ||(board[0] != null && board[0] == board[3] && board[0] == board[6])
				 ||(board[1] != null && board[1] == board[4] && board[1] == board[7])
				 ||(board[2] != null && board[2] == board[5] && board[2] == board[8])
				 ||(board[0] != null && board[0] == board[4] && board[0] == board[8])
				 ||(board[2] != null && board[2] == board[4] && board[2] == board[6]);
	 }
	 public static  String currentBoard() // board
	    {
	        System.out.println( "\n\n" );
	        System.out.println(  "\n\n" );
	        System.out.println(  "\n\n\t\t" + board [0] + "   | " +board [1]+ "  | " +board [2]);
	        System.out.println(  " \t\t    |    |   " );
	        System.out.println(  " \t\t ___|____|___ " );
	        System.out.println(  "\n\n\t\t" +board [3]+ "   | " +board [4]+ "  | " +board [5]);
	        System.out.println(  " \t\t    |    |   " );
	        System.out.println(  " \t\t ___|____|___ " );
	        System.out.println(  "\n\n\t\t" +board [6]+ "   | " +board [7]+ "  | " +board [8]);
	        System.out.println(  " \t\t    |    |   " );
	        System.out.println(  " \t\t    |    |   " );
	        System.out.println(  "\n\n" );
	        return "currentBoard";
	    }
	 
	 
	 
	 static boolean putTheLetter(int choice){  // is there some place?
		 if(board[choice]==null) {board[choice]="X";
		 return true;
		 }
		 return false;
	 }
	 
	 static void putTheEnemyLetter(int choice) {
		 if(board[choice]==null) {
			 board[choice]="O";
		 } else System.out.println("Smth wrong!!!");
	 }
	 
	 
	 
	 public static boolean boardFilledUp() {    // tie7
		 for (int i = 0; i < board.length; i++) {
			 if (board[i] == null) {
				 return false;
			 }
		 }
		 return true;
	 }
	 
	 static int  game()  {     //1. check victory or tie.2 choose the number. 3 put the number on the board;
		 int choice=-1;
		 try {
			 while(choice<0||choice>8) {
				 System.out.println("Plz, write number from 0 to 8!");
			 choice = Integer.parseInt(read.readLine());
			 }
		 
			 
			 
			 
			 
		 while(!putTheLetter(choice)) {     
			 System.out.println("Chose other cell! this cell has a value! ");
			 choice=-1;
			 while(choice<0||choice>8) {
				 System.out.println("Plz, write number from 0 to 8!");
			 choice = Integer.parseInt(read.readLine());
			 }
		 }
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
		 
		 
		 
		 
		 
		 if(hasWinner()==true) {
			 System.out.println("There is Victory!");
		 } else if(boardFilledUp()==true) {
				 System.out.println("Tie!");
                  }
		 
			 return choice;
		 }
	 
	 
	 
	public static void main(String[] args) {


		 int port = 6666;
	       try {
	         ServerSocket ss = new ServerSocket(port); 
	         System.out.println("Waiting for a client...!!!");

	         Socket socket = ss.accept(); 
	         System.out.println("Opponent is here!");
	         System.out.println();

	         InputStream sin = socket.getInputStream();
	         OutputStream sout = socket.getOutputStream();

	 
	         DataInputStream in = new DataInputStream(sin);
	         DataOutputStream out = new DataOutputStream(sout);
	      
	         
	        
	        while(true){
	        	currentBoard();
	        	System.out.println("Make your choice.");
	        	int buf= game();
	        	currentBoard();

	           out.writeInt(buf); 
	           out.flush();
	        
	           int bufOfEnemy=in.readInt();
	           putTheEnemyLetter(bufOfEnemy);
	           currentBoard();
			}
	      } catch(Exception x) { x.printStackTrace(); }
	}
	
	
	




















}