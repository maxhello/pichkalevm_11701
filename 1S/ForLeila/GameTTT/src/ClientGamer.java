import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientGamer  {

	static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
	 private static String[] board = {
		        null, null, null,
		        null, null, null,
		        null, null, null};
	
	 public static boolean hasWinner() {  
		 return
				 (board[0] != null && board[0] == board[1] && board[0] == board[2])
				 ||(board[3] != null && board[3] == board[4] && board[3] == board[5])
				 ||(board[6] != null && board[6] == board[7] && board[6] == board[8])
				 ||(board[0] != null && board[0] == board[3] && board[0] == board[6])
				 ||(board[1] != null && board[1] == board[4] && board[1] == board[7])
				 ||(board[2] != null && board[2] == board[5] && board[2] == board[8])
				 ||(board[0] != null && board[0] == board[4] && board[0] == board[8])
				 ||(board[2] != null && board[2] == board[4] && board[2] == board[6]);
	 }
	 public static  String currentBoard() // board
	    {
			String line = "";
			System.out.println();
			line = " " + board[0] + " | " + board[1] + " | " +board[2];
			System.out.println(line);
			System.out.println("-----------");
			line = " " + board[3] + " | " + board[4] + " | " + board[5];
			System.out.println(line);
			System.out.println("-----------");
			line = " " + board[6] + " | " + board[7] + " | " + board[8];
			System.out.println(line);
			System.out.println();
	        return "currentBoard";
	    }
	 
	 
	 
	 static boolean putTheLetter(int choice){  // is there some place?
		 if(board[choice]==null) {board[choice]="O";
		 return true;
		 }
		 return false;
	 }
	 
	 static void putTheEnemyLetter(int choice) {
		 if(board[choice]==null) {
			 board[choice]="X";
		 } else System.out.println("Smth wrong!!!");
	 }
	 
	 
	 
	 public static boolean boardFilledUp() {    // tie7
		 for (int i = 0; i < board.length; i++) {
			 if (board[i] == null) {
				 return false;
			 }
		 }
		 return true;
	 }
	 
	 static int  game()  {     //1. check victory or tie.2 choose the number. 3 put the number on the board;
		 int choice=-1;
		 try {
			 while(choice<0||choice>8) {
				 System.out.println("Plz, write number from 0 to 8!");
			 choice = Integer.parseInt(read.readLine());
			 }
		 
			 
			 
			 
			 
		 while(!putTheLetter(choice)) {     
			 System.out.println("Chose other cell! this cell has a value! ");
			 choice=-1;
			 while(choice<0||choice>8) {
				 System.out.println("Plz, write number from 0 to 8!");
			 choice = Integer.parseInt(read.readLine());
			 }
		 }
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
		 
		 
		 
		 
		 
		 if(hasWinner()==true) {
			 System.out.println("There is Victory!");
		 } else if(boardFilledUp()==true) {
				 System.out.println("Tie!");
                  }
		 
			 return choice;
		 }
	 
	 
	public static void main(String[] args) {

         String ip ="192.168.0.6";
		 int port = 6666;
	       try {
	         
	    	   InetAddress ipAddress = InetAddress.getByName(ip); 
	            System.out.println(" IP address " + ip + " and port " + port );
	            Socket socket = new Socket(ipAddress, port);
	         System.out.println("I am connected!");
	         System.out.println();

	         InputStream sin = socket.getInputStream();
	         OutputStream sout = socket.getOutputStream();

	 
	         DataInputStream in = new DataInputStream(sin);
	         DataOutputStream out = new DataOutputStream(sout);
	      
	         
	        
	        while(true){
	        	currentBoard();
	        	int bufOfEnemy=in.readInt();
	        	putTheEnemyLetter(bufOfEnemy);
	        	currentBoard();
	        	System.out.println("Make your choice.");
	        	int buf= game();
	        	currentBoard();

	           out.writeInt(buf); 
	           out.flush();
	        
			}
	      } catch(Exception x) { x.printStackTrace(); }
	}
}
	
